/*
 * im9.h
 *
 *  Created on: Aug 27, 2012
 *      Author: nusuav
 */

#ifndef IM9_H_
#define IM9_H_

//#include "state.h"
#include "svo.h"
#include "thread.h"

#define IM9_BAUDRATE	38400	/*57600*/
#define MAX_IM9PACK		128
#define MAX_IM9BUFFER	2048
#define MAX_IM9PACK		128
#define FULL_PACKET_SIZE	128


struct  IM9PACK {
	double a, b, c;
	double p, q, r;
	double acx, acy, acz;
	double ail, ele, thr, rud, tog;
};

class clsIM9 : public clsThread {
public:
	clsIM9();
	virtual ~clsIM9();

public:
	int m_nsIM9;

protected:
	int m_nIM9;
	BOOL first_time;
	IM9PACK first_pack;

	IM9PACK m_im90;
//	UAVSTATE m_state0;

	//to store received packs
	double m_tIM9[MAX_IM9PACK];
//	UAVSTATE m_state[MAX_IM9PACK];

//	HELICOPTERRUDDER m_MAN[MAX_IM9PACK];
	SVODATA m_MAN[MAX_IM9PACK];

	pthread_mutex_t m_mtxIM9;

public:
	BOOL MNAVUpdate(IM9PACK& pack);
	void DecodeIMUpacket(char* buffer, IM9PACK& pack);
	void DecodeGPSpacket(char* buffer);

public:
	IM9PACK GetIM9Pack() const { return m_im90;}
	virtual BOOL InitThread();
	virtual int EveryRun();
	virtual void ExitThread();

	friend class clsURG;
	friend class clsDLG;
};

#endif /* IM9_H_ */
