/*
 * singleAxisFusion.h
 *
 *  Created on: Jul 30, 2013
 *      Author: Peidong
 */

#ifndef SINGLEAXISFUSION_H_
#define SINGLEAXISFUSION_H_

#include "stdio.h"
#include "matrix.h"
#include "uav.h"

extern clsParser _parser;

struct SINGLEAXISSTATE{
	double estx;
	double estv;
	double esta;
	double estBias;
};

struct SINGLEAXISMEASUREMENT{
	BOOL bCorrectMeasurement;
	double ig500nZ;
	double laserZ;
	double ig500nWg;
	double ig500nAg;
};

class singleAxisFusion {
public:
	singleAxisFusion();
	virtual ~singleAxisFusion();

public:
	void Init(const SINGLEAXISSTATE initialState);
	void UpdateFilter(SINGLEAXISMEASUREMENT measurement);
	SINGLEAXISSTATE GetEstiState() { return m_fusionState;};

	void SetKalmanA(const char parameter[256]) { _parser.GetVariable(parameter, Kalman_A); clsMatrix::T(Kalman_A, Kalman_AT);} ;
	void SetKalmanC(const char parameter[256]) { _parser.GetVariable(parameter, Kalman_C); clsMatrix::T(Kalman_C, Kalman_CT);} ;
	void SetKalmanQ(const char parameter[256]) { _parser.GetVariable(parameter, Kalman_Q);} ;
	void SetKalmanRwLaser(const char parameter[256]) { _parser.GetVariable(parameter, Kalman_RwLaser); };
	void SetKalmanRwoLaser(const char parameter[256]) { _parser.GetVariable(parameter, Kalman_RwoLaser);};
	void SetKalmanStartT (double time0) { m_t0 = time0; };
	void SetKalmanInitializationPeriod (double iniPeriod) { m_periodInitialization = iniPeriod;};
	void SetLaserLostTH( double threshold) {m_laserLostTh = threshold;};

	BOOL GetFilterStatus() { return m_bFilterReady; }

	void SetInnovationFilterCovariance(double cov) { m_nInnovationFilterCov = cov; }
protected:
	SINGLEAXISSTATE m_fusionState;
	BOOL m_bLaserFail;
	BOOL m_bFilterReady;
	double m_t0, m_periodInitialization;
	double m_laserLostTh;
	int m_nLaserFailCounter;

	double _Kalman_X[4]; clsVector Kalman_X;
	double _Kalman_A[4][4];	clsMatrix Kalman_A; clsMatrix Kalman_AT;
	double _Kalman_C[4][4];	clsMatrix Kalman_C; clsMatrix Kalman_CT;
	double _Kalman_Q[4][4];	clsMatrix Kalman_Q;
	double _Kalman_RwLaser[4][4];	clsMatrix Kalman_RwLaser;
	double _Kalman_RwoLaser[4][4];	clsMatrix Kalman_RwoLaser;
	double _Kalman_R[4][4]; clsMatrix Kalman_R;
	double _Kalman_Y[4]; clsVector Kalman_Y;
	double _Kalman_P[4][4]; clsMatrix Kalman_P;
	clsMatrix Kalman_K;
	double m_nInnovationFilterCov;
};

#endif /* SINGLEAXISFUSION_H_ */
