//ctl.cpp
//implementation file for automatic control
#include <stdio.h>
#include <pthread.h>
//#include <iostream>
//#include <cstdio>
//#include <cstdlib>

#include "matrix.h"
#include "uav.h"
#include "ctl.h"
#include "cmm.h"
#include "daq.h"
#include "parser.h"
#include "svo.h"
#include "state.h"


extern clsParser _parser;
extern clsCMM _cmm;
extern clsState _state;
extern EQUILIBRIUM _equ_Hover;



clsCTL::clsCTL()
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxCmd, &attr);

	pthread_mutex_init(&m_mtxCTL, &attr);
}

clsCTL::~clsCTL()
{
	pthread_mutex_destroy(&m_mtxCmd);
	pthread_mutex_destroy(&m_mtxCTL);
}

void clsCTL::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	m_cmd = *pCmd;

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsCTL::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if (m_cmd.code == 0) pCmd->code = 0;
	else {
		*pCmd = m_cmd;
		m_cmd.code = 0;				//clear
	}
	pthread_mutex_unlock(&m_mtxCmd);
}

BOOL clsCTL::InitThread()
{
	//for receiving command
	m_cmd.code = 0;

	//plan
	m_pPlan = NULL;				//no plan used currently

	m_tNotify = -1;				//no datalink notify in default, activated once notify command is received
	m_bNotify = FALSE;

	m_bGPSDeny = FALSE;		// use GPS by default
	m_bUseRefInOuter = FALSE;	// use practical data by default

	m_behavior.behavior = 0;				//no behavior
	m_fControl = 0;				//no control

	m_camera = 0;

	m_nCTL = 0;

	printf("[CTL] Start\n");
	return TRUE;
}

int clsCTL::EveryRun()
{
//	usleep(5000);
	char msg[256];
	//model simulation
	if (_state.GetSimulationType() != 0)
	{
		_state.Simulate();
//		_state.Filter();
//		_state.Observe();	// observer put in the innerloop
	}
	/*
	 * Get the control mode from manual input
	 */
	ProcessCtlMode();

	//check user command
	COMMAND cmd;
	GetCommand(&cmd);

	if (cmd.code != 0) {
		if (ProcessCommand(&cmd)) cmd.code = 0;
	}
	//if any command processed, the determined behavior to be execute is stored in m_behavior

	//check if datalink is lost
	if (m_tNotify > 0 && ::GetTime()-m_tNotify > 5 && m_bNotify) {
		m_bNotify = FALSE;				//indicate no notify signal, only response once the datalink is lost
	}

	// deal with the behavior rcved from ProcessCommand() instead of plan
	//if behavior specified by user commands or datalink lose, cancel & reset plan
	if (m_behavior.behavior != 0)
	{
		if (m_pPlan != NULL) {
			printf("Reset plan \n");
			m_pPlan->Reset();
		}
		printf("Set plan NULL \n");
		m_pPlan = NULL;
	}

	//if plan not null, get the next behavior according to the plan
	if (m_pPlan != NULL) {
		m_pPlan->SetState(&_state.GetState());				//store the initial state when the plan begins
		BOOL bEnd = m_pPlan->Run() == 0;
		m_pPlan->GetBehavior(&m_behavior);		// assign new behavior scheduled by the plan to clsCTL::m_behavior
		if (bEnd) {
			printf("plan end \n");
			printf("Reset plan \n");
			m_pPlan->Reset();
			m_pPlan = NULL;
		}
	}

	int nBehaviorThis = m_behavior.behavior;				//for purpose of save

	//execute behavior, dispatch behavior to corresponding control functions
	if (m_behavior.behavior != 0) 		// new behavior assigned
	{
		SetBehavior(&m_behavior);

		m_behaviorCurrent = m_behavior;
		m_behavior.behavior = 0;		//clear to continue the execution of the previous behavior

		UAVSTATE &state = _state.GetState();
		_state.Setc0(state.c);
		::sprintf(msg, "Behavior %d(%s)", m_behaviorCurrent.behavior, GetBehaviorString(m_behaviorCurrent.behavior));
		_cmm.PutMessage(msg);
	}


	//execute control functions
	if (m_fControl & CONTROLFLAG_OUTERLOOP_QUADLION) Outerloop_QuadLion();
	if (m_fControl & CONTROLFLAG_A8) A8();				//engine up or down
	if (m_fControl & CONTROLFLAG_INNERLOOP_QUADLION) Innerloop_QuadLion();
	if (m_fControl & CONTROLFLAG_MANUAL_INNERLOOP) Innerloop_Manual();
	if (m_fControl & CONTROLFLAG_OUTERLOOP_SEMI) Outerloop_Semi();
	if (m_fControl & CONTROLFLAG_INNERLOOP_SEMI) Innerloop_Semi();
	//update&store control signals
//	HELICOPTERRUDDER *pSig = m_fControl == 0 ? NULL : &m_sig;
	HELICOPTERRUDDER *pSig;
	int nMode = _svo.GetCurrentCTLMode();
	int nUAVStatus = _ctl.GetUAVStatus();
/*	if (m_nCount % 50 == 0) {
		printf("UAV Status %d\n", nUAVStatus);
	}*/
	if (nMode == CTL_MODE_AUTO) {
		pSig = &m_sig;
	}
	else if (nMode == CTL_MODE_SEMIAUTO /*&& nUAVStatus == STATUS_AIR*/) {
		pSig = &m_semiSig;
//		printf("Semiauto mode with semi signals\n");
	}
	else if (nMode == CTL_MODE_SEMIAUTO /* &&  nUAVStatus == STATUS_GROUND */) {
		// AUTO autosemi takeoff
//		::memset(&m_sig, 0, sizeof(m_sig));
		pSig = &m_sig;
//		printf("Semiauto mode with auto signals, throttle %.3f\n", pSig->throttle);
	}
	else if (nMode == CTL_MODE_MANUAL) {
		pSig = &m_manualSig;
	}
	_state.UpdateSIG(pSig);
//	printf("[SVO] m_sig: aileron %.3f, elevator %.3f, throttole %.3f\n", m_sig.aileron, m_sig.elevator, m_sig.throttle);

	CAM_PANTILT camPanTilt = {0};
	camPanTilt.pan = m_ctlPan; camPanTilt.tilt = m_ctlTilt;
	_state.SetCameraPanTilt(camPanTilt);

//	printf("pnr: B5_pnr[0] %.3f, B5_pnr[1] %.3f, B5_pnr[2] %.3f\n", B5_pnr[0], B5_pnr[1], B5_pnr[2]);
	double t = GetTime();
	UTCTIME utcTime = _state.GetUTCTime();

	SAFETY_STATUS safetyStatus = _state.GetAllSafetyStatus();

	pthread_mutex_lock(&m_mtxCTL);
	if (m_nCTL != MAX_CTL)
	{
		m_tCTL[m_nCTL] = t;
		m_ctl[m_nCTL].nBehavior = nBehaviorThis;
		m_ctl[m_nCTL].fControl = m_fControl;

		if ( (m_innerloop == INNERLOOP_LQR) || (m_innerloop == INNERLOOP_CNF) || (m_innerloop == INNERLOOP_GAINSCHEDULE) )
		{
			m_ctl[m_nCTL].u = A1A2A3_u;
			m_ctl[m_nCTL].v = A1A2A3_v;
			m_ctl[m_nCTL].w = A1A2A3_w;
			m_ctl[m_nCTL].r = A1A2A3_r;
		}
		else if (m_innerloop == INNERLOOP_RPT) {
			m_ctl[m_nCTL].u = B_acxr_ub;
			m_ctl[m_nCTL].v = B_acyr_vb;
			m_ctl[m_nCTL].w = B_aczr_wb;
			m_ctl[m_nCTL].r = B_cr;
		}
		else if (m_innerloop == INNERLOOP_DECOUPLE)
		{
			m_ctl[m_nCTL].u = A5_u;
			m_ctl[m_nCTL].v = A5_v;
			m_ctl[m_nCTL].w = A5_w;
			m_ctl[m_nCTL].r = A5_c;
		}

		m_ctl[m_nCTL].u = B5_vnr[0];
		m_ctl[m_nCTL].v = B5_vnr[1];
		m_ctl[m_nCTL].w = B5_vnr[2];
		m_ctl[m_nCTL].r = 0;

		m_ctl[m_nCTL].vChirp[0] = B5_pnr[0]; //B5_xref;
		m_ctl[m_nCTL].vChirp[1] = B5_pnr[1]; //B5_yref;
		m_ctl[m_nCTL].vChirp[2] = B5_pnr[2]; //B5_zref;
		m_ctl[m_nCTL].vChirp[3] = B5_pnr[3]; //B5_cref;

	    // push utc time to record
		m_ctl[m_nCTL].tmp[0] = (unsigned short)utcTime.hour; m_ctl[m_nCTL].tmp[1] = (unsigned short)utcTime.minutes; m_ctl[m_nCTL].tmp[2] = (unsigned short)utcTime.seconds;
		m_ctl[m_nCTL].tmp[3] = utcTime.nanoseconds;

		m_ctl[m_nCTL].tmp[4] = safetyStatus.bGPS; m_ctl[m_nCTL].tmp[5] = safetyStatus.bIMUData; m_ctl[m_nCTL].tmp[6] = safetyStatus.bIMURcv;
		m_ctl[m_nCTL].tmp[7] = safetyStatus.bPixhawkData; m_ctl[m_nCTL].tmp[8] = safetyStatus.bPixhawkRcv; //m_ctl[m_nCTL].tmp[9] = Vdesireall[8];

//		m_ctl[m_nCTL].tmp[10] = Pmeasureall[3]; m_ctl[m_nCTL].tmp[11] = Pmeasureall[4]; m_ctl[m_nCTL].tmp[12] = Pmeasureall[5];
//		m_ctl[m_nCTL].tmp[13] = Vmeasureall[3]; m_ctl[m_nCTL].tmp[14] = Vmeasureall[4]; m_ctl[m_nCTL].tmp[15] = Vmeasureall[5];

		m_nCTL ++;
	}
	pthread_mutex_unlock(&m_mtxCTL);

	return TRUE;
}

void clsCTL::ExitThread()
{
	delete	OP;
	delete	IP;
	delete	RML;

	OP = NULL;
	IP = NULL;
	RML = NULL;
	printf("[CTL] quit\n");
}

BOOL clsCTL::ProcessCommand(COMMAND *pCommand)
{
//	cout<<"clsCTL::ProcessCommand"<<endl;
	COMMAND &cmd = *pCommand;
	char *paraCmd = cmd.parameter;

	int &behavior = m_behavior.behavior;
	char *paraBeh = m_behavior.parameter;

	BOOL bProcess = TRUE;

	switch (cmd.code) {
	case COMMAND_TAKEOFF: {
		memcpy(paraBeh, paraCmd, 1*sizeof(double));
		double heightTmp = GETDOUBLE(paraCmd);
		SetTakeoffHeight(heightTmp);
		_ctl.SetPlan(2);
		m_pPlan->SetPlanID(2);
//		behavior = BEHAVIOR_TAKEOFF;
		break; }

	case COMMAND_LAND:
//		behavior = BEHAVIOR_LAND;
		_ctl.SetPlan(3);
		m_pPlan->SetPlanID(3);
		break;
	case COMMAND_ENGINEUP:
		behavior = BEHAVIOR_ENGINEUP;
		break;
	case COMMAND_ENGINEDOWN:
		behavior = BEHAVIOR_ENGINEDOWN;
		break;

	case COMMAND_ENGINE:
		behavior = BEHAVIOR_ENGINE;
		memcpy(paraBeh, paraCmd, sizeof(double));				//value of engine
		break;

	case COMMAND_HFLY: {
		behavior = BEHAVIOR_HFLY;
		memcpy(paraBeh, paraCmd, 4*sizeof(double));				//x, y, z, c - heading frame
		break;
	}

	case COMMAND_TEST: {

	}


	case COMMAND_PLAN: {
		int nPlan = GETLONG(paraCmd);
//		double para = GETDOUBLE(paraCmd + 4);
//		SetPlanPara(para);
		printf("[CTL] Command plan received, nPlan %d\n", nPlan);

		_ctl.SetPlan(nPlan);
		m_pPlan->SetPlanID(nPlan);

		break;
	}

	case COMMAND_NOTIFY:
		m_tNotify = ::GetTime();
		m_bNotify = TRUE;
		break;

	case COMMAND_NONOTIFY:
		m_tNotify = -1;
		m_bNotify = FALSE;
		break;

	case COMMAND_HOVER:
		behavior = BEHAVIOR_FLY;
		::memcpy(paraBeh, paraCmd, 4*sizeof(double));				//u,v,w,r
		break;

	case COMMAND_HOLD:
		behavior = BEHAVIOR_HOLD;
		memcpy(paraBeh, paraCmd, 4*sizeof(double));				//x,y,z,c
		break;

	case COMMAND_MODE: {
		int mode = GETLONG(paraCmd);
//		SetMode(mode);		// set to semi-auto as long as "mode" cmd received
		B5_outerloopMode = MODE_SEMIAUTO;
//		if (mode == MODE_SEMIAUTO)
			behavior = BEHAVIOR_SEMIAUTO;
			B5_bSemi1stFlag = TRUE;
		break; }

	case COMMAND_RETURNHOME: {
		SetPlan(5);
//		behavior = BEHAVIOR_RETURNHOME;
		break;
	}

	default:
		bProcess = FALSE;
		break;
	}

	return bProcess;
}


void clsCTL::A8()				//for engine up and down control (auto takeoff/landing)
{
	double t = ::GetTime()-A8_t0;

	if (A8_mode == A8MODE_ENGINEUP) {

		if (_HELICOPTER == ID_QUADLION) {
			if (_state.GetSimulationType() == 1) {
				// simulation mode
				double TUP = 4;
				BOOL bEnd = t >= TUP;
				if (m_behaviorCurrent.behavior == BEHAVIOR_ENGINEUP &&	bEnd && !A8_bEnd)
				{
					_state.SetEvent(EVENT_BEHAVIOREND, BEHAVIOR_ENGINEUP);
					printf("ENGINEUP event end at %.3f \n", ::GetTime());
				}
				A8_bEnd = bEnd;
			}
			else if (_state.GetSimulationType() == 0 || _state.GetSimulationType() == 2) {
				// practical flight mode
				m_sig.aileron = 0; m_sig.elevator = 0; m_sig.rudder = 0;
				double TUP = 3;
				// 0-4s increase throttle to 80% trim value
				if (t> 0 && t <= TUP) {
						m_sig.throttle = ( /*THROTTLE_LOW*/ 0*(TUP-t) + A2_equ.et*0.8*t)/TUP;
				}
				else if ( t> TUP ) {
						_state.SetEvent(EVENT_BEHAVIOREND, BEHAVIOR_ENGINEUP);
						printf("Rotor warm-up event end at %.3f \n", ::GetTime());
				}
			}
		}
	}

	if (A8_mode == A8MODE_ENGINEDOWN) {
		if (_HELICOPTER == ID_QUADLION) {
//			m_sig.throttle = -0.85;
//			_state.SetEvent(EVENT_BEHAVIOREND, BEHAVIOR_ENGINEDOWN);
			m_sig.aileron = 0; m_sig.elevator = 0; m_sig.rudder = 0; m_sig.throttle = 0;
			_state.SetEvent(EVENT_BEHAVIOREND, BEHAVIOR_ENGINEDOWN);
			printf("[ctl:A8-EngineDown] time %.2f: aileron %.3f, elevator %.3f, rudder %.3f, throttle %.3f\n", t, m_sig.aileron, m_sig.elevator, m_sig.rudder, m_sig.throttle);
		}

	}

	if (A8_mode == A8MODE_AUXILIARYDOWN) {
		m_sig.auxiliary = A8_sig0.auxiliary + 0.01*t;
		if (m_sig.auxiliary >= AUXILIARY_LOW) m_sig.auxiliary = AUXILIARY_LOW;
	}

#if (_DEBUG & DEBUGFLAG_CTL)
if (m_nCount % _DEBUG_COUNT == 0) {
	printf("[CTL] A8, eu %.3f, et %.3f\n", m_sig.auxiliary, m_sig.throttle);
}
#endif
}



void clsCTL::AutoPathGeneration()
{
	UAVSTATE state = _state.GetState();

	/// below is 2D control
	double tGet = ::GetTime();
	UTCTIME utcTime = _state.GetUTCTime();
	static BOOL bCheckTime = TRUE;

	if (bCheckTime) {
		m_bSyncStart = (utcTime.minutes == m_utcMin);
		if (m_bSyncStart)
			bCheckTime = FALSE;
	}

	if (GetTakeoffFlag()) {
		GetTakeoffRef();

		if (m_nCount % 50 == 0) {
			printf("Takeoff pnr ref: %f, %f, %f, %f \n", B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
			printf("Takeoff vnr ref: %f, %f, %f \n", B5_vnr[0], B5_vnr[1], B5_vnr[2]);
		}
	}
	else if (GetReturnHomeFlag()) {
		GetReturnHomeRef();
		if (m_nCount % 50 == 0) {
			printf("Return home pnr ref: %f, %f, %f, %f \n", B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
			printf("Return home vnr ref: %f, %f, %f \n", B5_vnr[0], B5_vnr[1], B5_vnr[2]);
		}
	}
	else if (GetHFLYFlag()) {
		GetHFLYRef();
		if (m_nCount % 50 == 0) {
			printf("HFLY pnr ref: %f, %f, %f, %f \n", B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
			printf("HFLY vnr ref: %f, %f, %f \n", B5_vnr[0], B5_vnr[1], B5_vnr[2]);
		}
	}
	else if (GetLandFlag()) {
		GetLandRef();
		if (m_nCount % 50 == 0) {
			printf("Land pnr ref: %f, %f, %f, %f \n", B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
			printf("Land vnr ref: %f, %f, %f \n", B5_vnr[0], B5_vnr[1], B5_vnr[2]);
		}
	}
	else if (GetOutdoorMultiWaypointFlag()) {
		DATASTRUCT_MASTERMIND_LASER2GUMSTIX l_data;



		if (outdoor_guide_init == false) {
			l_data.index = -1;
			l_data.x_r = 0;
			l_data.y_r = 0;
			l_data.z_r = GPSCoordinate[0][2];
			l_data.c_r = 0;
			l_data.flag_visited = false;
		} else {
			l_data.index = WaypointVisiting;
			l_data.x_r = WaypointOutdoorMultiWaypoint[WaypointVisiting][0];
			l_data.y_r = WaypointOutdoorMultiWaypoint[WaypointVisiting][1];
			l_data.z_r = WaypointOutdoorMultiWaypoint[WaypointVisiting][2];
			l_data.c_r = WaypointOutdoorMultiWaypoint[WaypointVisiting][3];

			double WaypntXDiff =
					WaypointOutdoorMultiWaypoint[WaypointVisiting][0] - state.x;
			double WaypntYDiff =
					WaypointOutdoorMultiWaypoint[WaypointVisiting][1] - state.y;
			double WaypntZDiff =
					WaypointOutdoorMultiWaypoint[WaypointVisiting][2] - state.z;
			double WaypntCDiff =
					WaypointOutdoorMultiWaypoint[WaypointVisiting][3] - state.c;
			INPI(WaypntCDiff);

			if (m_nCount % 50 == 0){
				printf( "waypoint : %f, %f, %f, %f\n" , WaypointOutdoorMultiWaypoint[WaypointVisiting][0] , WaypointOutdoorMultiWaypoint[WaypointVisiting][1],
						WaypointOutdoorMultiWaypoint[WaypointVisiting][2], WaypointOutdoorMultiWaypoint[WaypointVisiting][3]);
				printf( "state : %f, %f, %f, %f\n" , state.x , state.y,
						state.z, state.c);
				printf( "waypoint diff: %f, %f, %f, %f\n" , WaypntXDiff , WaypntYDiff,
					WaypntZDiff, WaypntCDiff);

			}

			double WPDiff = pow(WaypntXDiff, 2) + pow(WaypntYDiff, 2);

			if ((WPDiff < (1 ^ 2)) && (abs(WaypntZDiff) < 1) && (abs(
					WaypntCDiff) < (M_PI / 18))) {
				printf( "[MISSION] Arrived! Waypnt!!!!!!!!!!!!!!!!!!!!!!!!!\n" );
				l_data.flag_visited = true;
			} else {
				l_data.flag_visited = false;
			}

		}



		double xdiff = l_data.x_r - state.x;
		double ydiff = l_data.y_r - state.y;
		double dis = sqrt(xdiff * xdiff + ydiff * ydiff);
		double angle_diff = atan2(ydiff, xdiff) - state.c;
		//		if(n_outdoorWaypoints==0) return;

		if (WaypointVisiting >= n_outdoorWaypoints) {
			WaypointVisiting = 0;
			printf("[CTL] Outdoor MultiWaypoint visiting finished!\n");
			ResetOutdoorMultiWaypointFlag();
			char msg[256];
			::sprintf(msg, "Outdoor MultiWaypoint Navigation finished");
			_cmm.PutMessage(msg);
			SetIntegratorFlag();
			_state.SetEvent(EVENT_BEHAVIOREND, m_behaviorCurrent.behavior);
		}
		else {


			double xdiff, ydiff;

			if (outdoor_guide_init == false) {
				outdoor_guide_init = true;
				printf("number of waypoints: %d\n", n_outdoorWaypoints);
				for (int i = 0; i < n_outdoorWaypoints; i++) {
					xdiff = (GPSCoordinate[i][0] / 180 * PI - state.latitude)
							* 6.3781e6;
					//printf("coordinate lattitude: %lf\n", GPSCoordinate[i][0]);
					//printf("state lattitude: %lf\n", state.latitude);

					ydiff = (GPSCoordinate[i][1] / 180 * PI - state.longitude)
							* 6.3781e6;
					WaypointOutdoorMultiWaypoint[i][0] = xdiff + state.x;
					WaypointOutdoorMultiWaypoint[i][1] = ydiff + state.y;
					WaypointOutdoorMultiWaypoint[i][2] = GPSCoordinate[i][2];
					WaypointOutdoorMultiWaypoint[i][3] = GPSCoordinate[i][3]
							* PI / 180;
				}
			}
			else {
				// re-initialize the waypoints
				if (m_nCount % 500 == 0)// && state.GPS_fix == 1)
				{
					for (int i = 0; i < n_outdoorWaypoints; i++) {
						xdiff = (GPSCoordinate[i][0] / 180 * PI
								- state.latitude) * 6.3781e6;
						ydiff = (GPSCoordinate[i][1] / 180 * PI
								- state.longitude) * 6.3781e6;
						WaypointOutdoorMultiWaypoint[i][0] = xdiff + state.x;
						WaypointOutdoorMultiWaypoint[i][1] = ydiff + state.y;
						WaypointOutdoorMultiWaypoint[i][2]
								= GPSCoordinate[i][2];
						WaypointOutdoorMultiWaypoint[i][3]
								= GPSCoordinate[i][3] * PI / 180;
					}
				}
			}


			if (m_nCount % 50 == 0) {
				printf(
						"[MISSION] Outdoor MultiWaypoint is visiting the %d target at %lf %lf \n",
						WaypointVisiting,
						WaypointOutdoorMultiWaypoint[WaypointVisiting][0],
						WaypointOutdoorMultiWaypoint[WaypointVisiting][1]);

			}

			m_reflexRef.p_x_r = l_data.x_r;
			m_reflexRef.p_y_r = l_data.y_r;


			m_reflexRef.p_z_r = l_data.z_r;

			m_reflexRef.psi_r = l_data.c_r;
			//		ReflexxesPathPlanningStateUpdate(state, m_reflexRef, B5_pnr, B5_vnr, B5_anr);
			ReflexxesPathPlanning(m_reflexRef, B5_pnr, B5_vnr, B5_anr);
			/*         double angle_error = abs(state.c-B5_pnr[3]);
			 INPI(angle_error);
			 if(abs(state.x - B5_pnr[0])>1||abs(state.y - B5_pnr[1]>1)||abs(state.z - B5_pnr[2])>0.8||angle_error>0.4)
			 {
			 B5_pnr[0] = state.x;
			 B5_pnr[1] = state.y;
			 B5_pnr[2] = state.z;
			 B5_pnr[3] = state.c;
			 B5_vnr[0] = state.ug;
			 B5_vnr[1] = state.vg;
			 B5_vnr[2] = state.wg;
			 }
			 */
			if (m_nCount % 50 == 0) {
				printf("[MISSION](x,y,z,c): pnr ref: %f, %f, %f, %f \n",
						B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
				printf("[MISSION]  vnr ref: %f, %f, %f \n", B5_vnr[0],
						B5_vnr[1], B5_vnr[2]);
			}


			if (l_data.index == WaypointVisiting && l_data.flag_visited == true) {
				WaypointVisiting++;

				printf( "[MISSION] Target finished is %lf %lf %lf %lf\n",
						l_data.x_r, l_data.y_r, l_data.z_r, l_data.c_r);
			}

		}

	}
	else {	// hover
//		B5_xref = B5_pnr[0] = B5_x; B5_yref = B5_pnr[1] = B5_y; B5_zref = B5_pnr[2] = B5_z; B5_cref = B5_pnr[3] = B5_c;
		::memset(B5_vnr, 0, 3*sizeof(double));
		::memset(B5_anr, 0, 3*sizeof(double));

		if (m_nCount % 50 == 0) {
			printf("pnr ref: %f, %f, %f, %f \n", B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
			printf("vnr ref: %f, %f, %f \n", B5_vnr[0], B5_vnr[1], B5_vnr[2]);
		}
	}

}


void clsCTL::Innerloop_Manual()
{
	if (_state.GetSimulationType() == 1) {
		// simulation mode
		SVODATA &svo = _svo.GetSVOData();
	    m_manualSig.throttle = svo.throttle;
	    m_manualSig.elevator = svo.elevator;
	    m_manualSig.aileron  = svo.aileron;
	    m_manualSig.rudder   = svo.rudder;
	}
	else if (_state.GetSimulationType() == 0) {
		// practical flight mode
		SVODATA &svo = _svo.GetSVOData();
	    m_manualSig.throttle = svo.throttle / 2.0 + 0.5;
	    m_manualSig.elevator = svo.elevator;
	    m_manualSig.aileron  = svo.aileron;
	    m_manualSig.rudder   = svo.rudder;
	}
}

void clsCTL::Innerloop_QuadLion()
{
	if (_state.GetSimulationType() == 1) {
		// inner-loop in simulation mode
	      UAVSTATE &state = _state.GetState();
	//    double abc[3] = {state.a, state.b, state.c};
	      double abc[3] = {0, 0, state.c};
	//    double abcgr1[3] = {state.ug, state.vg, B_aczr_wb};
	      double abcgr1[3] = {0, 0, B_aczr_wb};
	      double abcbr1[3] = {0};
	      G2B(abc, abcgr1, abcbr1);

	      // acz in NED
	      double accb[3] = {state.acx, state.acy, state.acz};
	      double accg[3] = {0};
	      B2G(abc, accb, accg);
	//    double abcgr2[3] = {B_acxr_ub, B_acyr_vb, accg[2]};
	      double abcgr2[3] = {B_acxr_ub, B_acyr_vb, 0};
	      double abcbr2[3] = {0};
	      G2B(abc, abcgr2, abcbr2);

	//    abcbr2[0] = range(abcbr2[0], -MAX_ACCB, MAX_ACCB);
	//    abcbr2[0] = range(abcbr2[1], -MAX_ACCB, MAX_ACCB);
	      m_sig.throttle = A2_equ.et + abcbr1[2] / dc_thr2w;
	      m_sig.elevator = A2_equ.ee + ( state.u * damping_u + abcbr2[0] ) / ratio_u / dc_ele2tht;
	      m_sig.aileron  = A2_equ.ea + ( state.v * damping_v + abcbr2[1] ) / ratio_v / dc_ail2phi;
	      m_sig.rudder   = A2_equ.er + B_cr;
	}
	else if (_state.GetSimulationType() == 0 || _state.GetSimulationType() == 2) {
		// practical flight mode
		UAVSTATE &state = _state.GetState();
		double abc[3] = {0, 0, state.c};

		double abcgr2[3] = {B_acxr_ub, B_acyr_vb, 0};
		double abcbr2[3] = {0};
		G2B(abc, abcgr2, abcbr2);

		// Calculate throttle compensation
/*		double phiRef = abcbr2[1]/_gravity;
		double thetaRef = abcbr2[0]/_gravity;
		double product = cos(phiRef*0.64) * cos(thetaRef*0.64);
		double trimNew = 0;
		if (product <= 0.01) {
			trimNew = A2_equ.et;
		} else {
			trimNew = A2_equ.et / product;
		}

		m_sig.aileron  = A2_equ.ea + phiRef * 0.64;
		m_sig.elevator = A2_equ.ee - thetaRef * 0.64;
		m_sig.throttle = trimNew - B_aczr_wb*0.08;
		m_sig.rudder   = A2_equ.er + B_cr;*/

		m_sig.aileron  = A2_equ.ea + (abcbr2[1]/_gravity) * 0.64;
		m_sig.elevator = A2_equ.ee - (abcbr2[0]/_gravity) * 0.64;
		m_sig.throttle = A2_equ.et - B_aczr_wb*0.05; //0.08; //0.05; //0.095;
		m_sig.rudder   = A2_equ.er + B_cr;

		m_sig.aileron  = range(m_sig.aileron,  -0.45, 0.45);
		m_sig.elevator = range(m_sig.elevator, -0.45, 0.45);
		m_sig.throttle = range(m_sig.throttle,  0.0, 0.8);
		m_sig.rudder   = range(m_sig.rudder,   -1.0, 1.0);

		double t = ::GetTime();
	//	m_sig.laser     = 0.1+0.9*sin(0.2*PI*t); // trim + 45 deg to the left
	//	m_sig.auxiliary = 45.0*sin(0.2*PI*(t-0.1)); // delayed by 0.1s

		static double pre_ail = m_sig.aileron;
		static double pre_ele = m_sig.elevator;
		static double pre_thr = m_sig.throttle;
		static double pre_rud = m_sig.rudder;

		if (GetIntegratorFlag())
		{
	//		double temp1 = 0.9995;
	//		double temp2 = 0.00025;
	//		A2_equ.ea = temp1*A2_equ.ea + temp2*(m_sig.aileron+pre_ail);
	//		A2_equ.ee = temp1*A2_equ.ee + temp2*(m_sig.elevator+pre_ele);
	//		A2_equ.er = temp1*A2_equ.er + temp2*(m_sig.rudder+pre_rud);
	//		A2_equ.et = 0.9683*A2_equ.et + 0.0159*(m_sig.throttle+pre_thr);
			A2_equ.et = 0.99*A2_equ.et + 0.005*(m_sig.throttle+pre_thr);
		}

		pre_ail = m_sig.aileron;
		pre_ele = m_sig.elevator;
		pre_thr = m_sig.throttle;
		pre_rud = m_sig.rudder;
	}

}

void clsCTL::Outerloop_QuadLion()
{

	AutoPathGeneration();

	/* RPT outer-loop control law */

	UAVSTATE &state = _state.GetState();

	// calculate dt
	double t = ::GetTime();
	double dt = 0;
	if (B5_t4 < 0) {
		dt = 0;
	} else {
		dt = t - B5_t4;
	}
	B5_t4 = t;

	if (GetIntegratorFlag()) {
		double xerr = state.x - B5_pnr[0];
		m_xerrint += xerr*dt;
		m_xerrint = range(m_xerrint, XERRINT_MIN, XERRINT_MAX);

		double yerr = state.y - B5_pnr[1];
		m_yerrint += yerr*dt;
		m_yerrint = range(m_yerrint, YERRINT_MIN, YERRINT_MAX);

		double zerr = state.z - B5_pnr[2];
		m_zerrint += zerr*dt;
		m_zerrint = range(m_zerrint, ZERRINT_MIN, ZERRINT_MAX);

		double cerr = state.c - B5_pnr[3]; INPI(cerr);
		m_cerrint += cerr*dt;
		m_cerrint = range(m_cerrint, CERRINT_MIN, CERRINT_MAX);
	}
	else {
		m_xerrint = m_yerrint = m_zerrint = m_cerrint = 0;
	}

	if (_state.GetSimulationType() == 1) {
		// simulation mode
		double agxy_r[2] = {0};
		agxy_r[0] = state.x * _Fxy[3] + B5_pnr[0]*_Fxy[0] + state.ug *_Fxy[4] + B5_vnr[0]*_Fxy[1] +
				B5_anr[0] + m_xerrint*_Fxy[2];

		agxy_r[1] = state.y * _Fxy[3] + B5_pnr[1]*_Fxy[0] + state.vg *_Fxy[4] + B5_vnr[1]*_Fxy[1] +
				B5_anr[1] + m_yerrint*_Fxy[2];

		double wg_r = state.z*_FzSim[2] + B5_pnr[2]*_FzSim[0] + B5_vnr[2] + m_zerrint*_FzSim[1];
		double rud_temp = B5_pnr[3] - state.c;
		double rud = (INPI(rud_temp)*_Fc[0] + m_cerrint*_Fc[1]) / dc_rud2r;

		B_acxr_ub = agxy_r[0];
		B_acyr_vb = agxy_r[1];
		B_aczr_wb = wg_r;
		B_cr = rud;
	}
	else if (_state.GetSimulationType() == 0 || _state.GetSimulationType() == 2) {
		// practical flight mode
		double agxy_r[3] = {0};
		agxy_r[0] = state.x * _Fxy[3] + B5_pnr[0]*_Fxy[0] + state.ug *_Fxy[4] + B5_vnr[0]*_Fxy[1] +
				B5_anr[0] + m_xerrint*_Fxy[2];

		agxy_r[1] = state.y * _Fxy[3] + B5_pnr[1]*_Fxy[0] + state.vg *_Fxy[4] + B5_vnr[1]*_Fxy[1] +
				B5_anr[1] + m_yerrint*_Fxy[2];

		agxy_r[2] = state.z * _Fz[3] + B5_pnr[2]*_Fz[0] + state.wg *_Fz[4] + B5_vnr[2]*_Fz[1] +
				/*B5_anr[2]*/ + m_zerrint*_Fz[2];

		double rud_temp = B5_pnr[3] - state.c;
		B_cr = INPI(rud_temp)*_Fc[0]*0.3 ;//+ m_cerrint*_Fc[1];

		B_acxr_ub = agxy_r[0];
		B_acyr_vb = agxy_r[1];
		B_aczr_wb = agxy_r[2];
	}

	if (GetNoGPSFlag()) {
		B_acxr_ub = 0;
		B_acyr_vb = 0;
	}
}

void clsCTL::Outerloop_Semi()
{
	GetSemiAutoRefInVelocityMode();
	if (m_nCount % 50 == 0) {
		printf("SemiAuto pnr ref: %f, %f, %f, %f \n", B5_pnr[0], B5_pnr[1], B5_pnr[2], B5_pnr[3]);
		printf("SemiAuto vnr ref: %f, %f, %f \n", B5_vnr[0], B5_vnr[1], B5_vnr[2]);
		printf("SemiAuto B_aczr_wb %.3f\n", B_aczr_wb);
	}

	UAVSTATE &state = _state.GetState();
	// calculate dt
	double t = ::GetTime();
	double dt = 0;
	if (B5_t4 < 0) {
		dt = 0;
	} else {
		dt = ::GetTime() - B5_t4;
	}
	B5_t4 = t;

	if (/*GetIntegratorFlag()*/ FALSE) {
		double xerr = state.x - B5_pnr[0];
		m_xerrint += xerr*dt;
		m_xerrint = range(m_xerrint, XERRINT_MIN, XERRINT_MAX);

		double yerr = state.y - B5_pnr[1];
		m_yerrint += yerr*dt;
		m_yerrint = range(m_yerrint, YERRINT_MIN, YERRINT_MAX);

		double zerr = state.z - B5_pnr[2];
		m_zerrint += zerr*dt;
		m_zerrint = range(m_zerrint, ZERRINT_MIN, ZERRINT_MAX);

		double cerr = state.c - B5_pnr[3]; INPI(cerr);
		m_cerrint += cerr*dt;
		m_cerrint = range(m_cerrint, CERRINT_MIN, CERRINT_MAX);
	}
	else {
		m_xerrint = m_yerrint = m_zerrint = m_cerrint = 0;
	}

	if (_state.GetSimulationType() == 1) {
		// simulation mode
		double agxy_r[2] = {0};
		agxy_r[0] = state.x * _Fxy[3] + B5_pnr[0]*_Fxy[0] + state.ug *_Fxy[4] + B5_vnr[0]*_Fxy[1] +
				B5_anr[0] + m_xerrint*_Fxy[2];

		agxy_r[1] = state.y * _Fxy[3] + B5_pnr[1]*_Fxy[0] + state.vg *_Fxy[4] + B5_vnr[1]*_Fxy[1] +
				B5_anr[1] + m_yerrint*_Fxy[2];

		double wg_r = state.z*_FzSim[2] + B5_pnr[2]*_FzSim[0] + B5_vnr[2] + m_zerrint*_FzSim[1];
		double rud_temp = B5_pnr[3] - state.c;
		double rud = (INPI(rud_temp)*_Fc[0] + m_cerrint*_Fc[1]) / dc_rud2r;


		B_acxr_ub = agxy_r[0];
		B_acyr_vb = agxy_r[1];
		B_aczr_wb = wg_r;
		B_cr = rud;
	}
	else if (_state.GetSimulationType() == 0) {
		// flight mode
		double agxy_r[3] = {0};
		agxy_r[0] = state.x * _Fxy[3] + B5_pnr[0]*_Fxy[0] + state.ug *_Fxy[4] + B5_vnr[0]*_Fxy[1] +
				B5_anr[0] + m_xerrint*_Fxy[2];

		agxy_r[1] = state.y * _Fxy[3] + B5_pnr[1]*_Fxy[0] + state.vg *_Fxy[4] + B5_vnr[1]*_Fxy[1] +
				B5_anr[1] + m_yerrint*_Fxy[2];

		agxy_r[2] = state.z * _Fz[3] + B5_pnr[2]*_Fz[0] + state.wg *_Fz[4] + B5_vnr[2]*_Fz[1] +
				/*B5_anr[2]*/ + m_zerrint*_Fz[2];

		double rud_temp = B5_pnr[3] - state.c;
		B_cr = INPI(rud_temp)*_Fc[0] + m_cerrint*_Fc[1];

		B_acxr_ub = agxy_r[0];
		B_acyr_vb = agxy_r[1];
		B_aczr_wb = agxy_r[2];
	}
}

void clsCTL::Innerloop_Semi()
{
	UAVSTATE &state = _state.GetState();
	double abc[3] = {0, 0, state.c};

	double abcgr2[3] = {B_acxr_ub, B_acyr_vb, 0};
	double abcbr2[3] = {0};
	G2B(abc, abcgr2, abcbr2);

	m_semiSig.aileron  = A2_equ.ea + (abcbr2[1]/_gravity) * 0.64;
	m_semiSig.elevator = A2_equ.ee - (abcbr2[0]/_gravity) * 0.64;
	m_semiSig.throttle = A2_equ.et - B_aczr_wb*0.08; //0.05; //0.095;
	m_semiSig.rudder   = A2_equ.er + B_cr;

	if (m_nCount % 50 == 0) {
		printf("Semi sig: ea %.3f, ee %.3f, et %.3f, er %.3f\n", m_semiSig.aileron, m_semiSig.elevator,
				m_semiSig.throttle, m_semiSig.rudder);
	}

	m_semiSig.aileron  = range(m_semiSig.aileron,  -0.45, 0.45);
	m_semiSig.elevator = range(m_semiSig.elevator, -0.45, 0.45);
	m_semiSig.throttle = range(m_semiSig.throttle,  0.0, 0.8);
	m_semiSig.rudder   = range(m_semiSig.rudder,   -1.0, 1.0);

	double t = ::GetTime();
//	m_sig.laser     = 0.1+0.9*sin(0.2*PI*t); // trim + 45 deg to the left
//	m_sig.auxiliary = 45.0*sin(0.2*PI*(t-0.1)); // delayed by 0.1s

	static double pre_ail = m_semiSig.aileron;
	static double pre_ele = m_semiSig.elevator;
	static double pre_thr = m_semiSig.throttle;
	static double pre_rud = m_semiSig.rudder;

	if (GetIntegratorFlag())
	{
//		double temp1 = 0.9995;
//		double temp2 = 0.00025;
//		A2_equ.ea = temp1*A2_equ.ea + temp2*(m_semiSig.aileron+pre_ail);
//		A2_equ.ee = temp1*A2_equ.ee + temp2*(m_semiSig.elevator+pre_ele);
//		A2_equ.er = temp1*A2_equ.er + temp2*(m_semiSig.rudder+pre_rud);
//		A2_equ.et = 0.9683*A2_equ.et + 0.0159*(m_semiSig.throttle+pre_thr);
		A2_equ.et = 0.99*A2_equ.et + 0.005*(m_semiSig.throttle+pre_thr);
	}

	pre_ail = m_semiSig.aileron;
	pre_ele = m_semiSig.elevator;
	pre_thr = m_semiSig.throttle;
	pre_rud = m_semiSig.rudder;
}

void clsCTL::SetBehavior(BEHAVIOR *pBeh)
{
	int nBehavior = pBeh->behavior;
	char *para = pBeh->parameter;

#if (_DEBUG & DEBUGFLAG_CTL)
	printf("[CTL] New behavior %d\n", nBehavior);
#endif

//	if (nBehavior == BEHAVIOR_HFLY) _svo.SetLimit(0.02);
//	else _svo.SetLimit(0.05);				//damping for hfly

	switch (pBeh->behavior) {
	case BEHAVIOR_LAND:
	case BEHAVIOR_RETURNHOME:
	case BEHAVIOR_ENGINEUP:
	case BEHAVIOR_ENGINEDOWN:
		SetBehavior(nBehavior);
		break;

	case BEHAVIOR_ENGINE:			//engine
	case BEHAVIOR_TAKEOFF:
		SetBehavior(nBehavior, (double &)para[0]);
		break;


	case BEHAVIOR_HFLY:				//height keeping fly (x,y,z,c)
	case BEHAVIOR_HOLD:				//hold (x,y,z,c)
		SetBehavior(nBehavior, (double &)para[0], (double &)para[8], (double &)para[16], (double &)para[24]);
		break;


	case BEHAVIOR_TEST:				//test 1, servo driving
		printf("[Behavior] test!\n");
		SetBehavior(nBehavior, (int &)para[0]);
		break;


	case BEHAVIOR_SEMIAUTO:
		m_fControl = CONTROLFLAG_OUTERLOOP_SEMI | CONTROLFLAG_INNERLOOP_SEMI;
		break;

	}
}

void clsCTL::SetBehavior(int nBehavior)
{
	UAVSTATE &state = _state.GetState();

	switch (nBehavior) {
	case BEHAVIOR_ENGINEUP:
	case BEHAVIOR_ENGINEDOWN:
		m_fControl = CONTROLFLAG_A8;
		A1A2A3_u = A1A2A3_v = A1A2A3_w = A1A2A3_r = 0;
		A8_t0 = ::GetTime();
		A8_sig0 = m_sig;
		if (nBehavior == BEHAVIOR_ENGINEUP)
			A8_mode = A8MODE_ENGINEUP;
		else
			A8_mode = A8MODE_ENGINEDOWN;
		break;

	case BEHAVIOR_LAND: {
//		memset(&m_reflexRef, 0, sizeof(m_reflexRef));
//		m_reflexRef.p_x_r = state.x; m_reflexRef.p_y_r = state.y; m_reflexRef.p_z_r = -5;
//		m_reflexRef.psi_r = state.c;
		m_reflexRef.p_x_r = B5_pnr[0]; m_reflexRef.p_y_r = B5_pnr[1]; //m_reflexRef.psi_r = state.c;
		m_reflexRef.p_z_r = -7;
		m_reflexRef.v_z_r = 0.3;
		ReflexxesPathPlanning(state, m_reflexRef, m_tLand, B5_pnr, B5_vnr, B5_anr);
		printf("Land time needed %.3f \n", m_tLand);
		m_fControl = CONTROLFLAG_OUTERLOOP_QUADLION | CONTROLFLAG_INNERLOOP_QUADLION;
		SetLandFlag();
		B5_t0 = ::GetTime();
//		m_bLandCmd = TRUE;
		break; }


	case BEHAVIOR_RETURNHOME: {
		double x, y;
		GetTakeoffPos(x, y);
//		double dx = x - B5_pnr[0] /*state.x*/; double dy = y - B5_pnr[1]; //state.y;
//		double c = atan2(dy, dx); INPI(c);

//		memset(&m_reflexRef, 0, sizeof(m_reflexRef));
		m_reflexRef.p_x_r = x; m_reflexRef.p_y_r = y; m_reflexRef.p_z_r = B5_pnr[2];
		m_reflexRef.psi_r = B5_pnr[3];
		ReflexxesPathPlanning(state, m_reflexRef, m_tReturnHome, B5_pnr, B5_vnr, B5_anr);
		printf("Return home time needed %.3f \n", m_tGPSPath);

		m_fControl = CONTROLFLAG_OUTERLOOP_QUADLION | CONTROLFLAG_INNERLOOP_QUADLION;
		SetReturnHomeFlag();
		B5_t0 = ::GetTime();
		break;
		}

	}
}

void clsCTL::SetBehavior(int nBehavior, int nTest)
{
	//BEHAVIOR_TEST(test)
	UAVSTATE &state = _state.GetState();

	if (nTest == 18) {
		SetIndoorMultiWaypointFlag();
		printf("[Full Path] indoor nav set!\n");
	} else if (nTest == 20) {
		SetOutdoorMultiWaypointFlag();
	}
	else {
		m_fControl = CONTROLFLAG_A4;
		A4_nTest = nTest;
	}
}

void clsCTL::SetBehavior(int nBehavior, double d1)
{
	UAVSTATE &state = _state.GetState();

	switch (nBehavior) {
	case BEHAVIOR_ENGINE: {
		m_sig.aileron = m_sig.elevator = m_sig.rudder = m_sig.throttle = 0;
		printf("[CTL] Engine low\n");
		break;
	}
	case BEHAVIOR_TAKEOFF: {
		SetTakeoffPos(state.x, state.y);
		int nUAVStatus = _ctl.GetUAVStatus();
		memset(&m_reflexRef, 0, sizeof(m_reflexRef));
		if (_svo.GetCurrentCTLMode() == CTL_MODE_AUTO) {
			if (GetTakeoffAdjustFlag()) {
				// translate from body offset to NED for followers
				double phr[3] = {m_takeOffsetX, m_takeOffsetY, 0};
				double pnr[3] = {0};
				double c0 = m_cLeader;
				double abcg[3] = {0, 0, c0};
				B2G(abcg, phr, pnr);

				double dc = m_cLeader - state.c; INPI(dc);
//				m_reflexRef.p_x_r = pnr[0]; m_reflexRef.p_y_r = pnr[1];

				m_reflexRef.p_z_r = -d1;
				// start take-off from the current state of x,y to avoid the drifting to (0,0)
				m_reflexRef.p_x_r = state.x; m_reflexRef.p_y_r = state.y;
				// only adjust heading during the takeoff
				m_reflexRef.psi_r = state.c + dc;
			}
			else {
				m_reflexRef.p_x_r = state.x; m_reflexRef.p_y_r = state.y; m_reflexRef.p_z_r = -d1;
				m_reflexRef.psi_r = state.c;
			}
		}
		else if (_svo.GetCurrentCTLMode() == CTL_MODE_SEMIAUTO && nUAVStatus == STATUS_GROUND) {
			m_reflexRef.p_x_r = state.x; m_reflexRef.p_y_r = state.y; m_reflexRef.p_z_r = -d1;
			m_reflexRef.psi_r = state.c;
		}

		ReflexxesPathPlanning(state, m_reflexRef, m_tTakeoff, B5_pnr, B5_vnr, B5_anr);
		printf("Take off time needed %.3f \n", m_tTakeoff);
		m_fControl = CONTROLFLAG_OUTERLOOP_QUADLION | CONTROLFLAG_INNERLOOP_QUADLION;
		SetTakeoffFlag();
		B5_t0 = ::GetTime();
		break;
		}
	}


}


void clsCTL::SetBehavior(int nBehavior, double d1, double d2, double d3, double d4)
{
	//BEHAVIOR_H5, H10, H11, FLY, HOLD
	UAVSTATE &state = _state.GetState();

	double u, v, w, x, y, z, r, c;
	switch (nBehavior) {
	case BEHAVIOR_HFLY: {
		x = d1 > MAXVALUE ? state.x : d1;
		y = d2 > MAXVALUE ? state.y : d2;
		z = d3 > MAXVALUE ? state.z : d3;
		c = d4 > MAXVALUE ? state.c : d4;

		// transform from local heading frame(x,y) to local NED frame for "hfly(x,y,z,c)" command
		double phr[4] = {x, y, z, c};
		double pnr[4] = {0};
		double c0 = state.c;
		double abcg[3] = {0, 0, c0};
		B2G(abcg, phr, pnr);

		memset(&m_reflexRef, 0, sizeof(m_reflexRef));

		if (/*_svo.GetCTLMode() == CTL_MODE_TRANSIT_SEMI2AUTO || */_svo.GetCTLMode() == CTL_MODE_TRANSIT_MANUAL2AUTO) {
			m_reflexRef.p_x_r = state.x; m_reflexRef.p_y_r = state.y; m_reflexRef.p_z_r = state.z;
			m_reflexRef.psi_r = state.c;
		}
		else if (_svo.GetCTLMode() == CTL_MODE_TRANSIT_SEMI2AUTO) {
			m_reflexRef.p_x_r = B5_pnr[0]; m_reflexRef.p_y_r = B5_pnr[1]; m_reflexRef.p_z_r = B5_pnr[2];
			m_reflexRef.psi_r = B5_pnr[3];
			m_reflexRef.v_x_r = B5_vnr[0]; m_reflexRef.v_y_r = B5_vnr[1]; m_reflexRef.v_z_r = B5_vnr[2];
		}
		else {
			printf("hfly ********mode 2*****\n");
			B5_pnr[0] = B5_pnr[0] + pnr[0];
			B5_pnr[1] = B5_pnr[1] + pnr[1];
			B5_pnr[2] = B5_pnr[2] + pnr[2];
			B5_pnr[3] = B5_pnr[3] + c*PI/180; //INPI(B5_pnr[3]);

			m_reflexRef.p_x_r = B5_pnr[0]; m_reflexRef.p_y_r = B5_pnr[1]; m_reflexRef.p_z_r = B5_pnr[2];
			m_reflexRef.psi_r = B5_pnr[3];
		}

		ReflexxesPathPlanning(state, m_reflexRef, m_tHFLY, B5_pnr, B5_vnr, B5_anr);
		printf("HFLY time needed %.3f \n", m_tHFLY);

		m_fControl = CONTROLFLAG_OUTERLOOP_QUADLION | CONTROLFLAG_INNERLOOP_QUADLION;
		SetHFLYFlag();
		B5_t0 = ::GetTime();
		break;}



	case BEHAVIOR_HOLD:

		x = d1 > MAXVALUE ? state.x : d1;
		y = d2 > MAXVALUE ? state.y : d2;
		z = d3 > MAXVALUE ? state.z : d3;
		c = d4 > MAXVALUE ? state.c : d4;

		x = range(x, MIN_STATE_X, MAX_STATE_X);
		y = range(y, MIN_STATE_Y, MAX_STATE_Y);
		z = range(z, MIN_STATE_Z, MAX_STATE_Z);

		INPI(c);

		if (m_innerloop == INNERLOOP_RPT) {
			m_fControl = CONTROLFLAG_B5 | CONTROLFLAG_A2;

			B5_x = x; B5_y = y; B5_z = z; B5_c = c; B5_pPath = B5_pPath2 = NULL;
			B5_t0 = ::GetTime();
			B5_t = B5_t0;
			B5_dxi = B5_dyi = B5_dzi = B5_dci = 0;
			B5_bEnd = FALSE;

			if (_HELICOPTER == ID_QUADLION) {
				m_fControl = CONTROLFLAG_OUTERLOOP_QUADLION | CONTROLFLAG_INNERLOOP_QUADLION;
				SetReflexxesInitFlag();	// set to reinitialize reflexxes path planning
				B5_pnr[0] = x; B5_pnr[1] = y; B5_pnr[2] = z; B5_pnr[3] = c;
				B5_vnr[0] = B5_vnr[1] = B5_vnr[2] = 0;
				B5_anr[0] = B5_anr[1] = B5_anr[2] = 0;
			}


		}
		else {

			B2_x = x; B2_y = y; B2_z = z; B2_c = c; B2_pPath = NULL;
			B2_t0 = ::GetTime();
			B2_t = B2_t0;
			B2_dxi = B2_dyi = B2_dzi = B2_dci = 0;
			B2_bEnd = FALSE;
		}
		break;



	}
}




void clsCTL::Init()
{
	// Li Jiaxin
	// read outdoor.txt from disk
	ResetFlag_FULL_PLAN();
	FileOutdoorMultiWaypoint = fopen("outdoor.txt", "r");
	if (FileOutdoorMultiWaypoint != NULL) {
		fscanf(FileOutdoorMultiWaypoint, "%d", &n_outdoorWaypoints);
		printf("[CTL] outdoor waypoint number is %d \n", n_outdoorWaypoints);
		for (int i = 0; i < n_outdoorWaypoints; i++) {
			printf("[CTL]GPS coordinate is");
			for (int j = 0; j < 4; j++) {
				fscanf(FileOutdoorMultiWaypoint, "%lf", &GPSCoordinate[i][j]);
				printf("%lf ", GPSCoordinate[i][j]);
			}
			printf("\n");
		}
	} else {
		n_outdoorWaypoints = 0;
	}
	outdoor_guide_init = false;
	WaypointVisiting = 0;

	// debug read txt
	n_outdoorWaypoints = 2;
	GPSCoordinate[0][0] = 1.329025;
	GPSCoordinate[0][1] = 103.787316;
	GPSCoordinate[0][2] = -15;
	GPSCoordinate[0][3] = 180;

	GPSCoordinate[1][0] = 1.3285896;//1.328403;//1.327781;
	GPSCoordinate[1][1] = 103.7869688;//103.78682;//103.786324;
	GPSCoordinate[1][2] = -15;
	GPSCoordinate[1][3] = 180;

	m_bFK = FALSE;

	m_innerloop = INNERLOOP_RPT;
	m_nHeading = 0;
	m_equ = _equ_Hover;
	::memset(&m_sig, 0, sizeof(m_sig));

// A1 equ and F, G are dynamically decided by A1_Lookup (gain schedule)
	A2_equ = _equ_Hover;
	if ( _HELICOPTER == ID_QUADLION ) {
		m_xerrint = m_yerrint = m_zerrint = m_cerrint = 0;
		OUTER_P_QUADLION.Reset(4,7, (double *)_OUTER_P_QUADLION, TRUE);
		OUTER_D_QUADLION.Reset(4,7, (double *)_OUTER_D_QUADLION, TRUE);
		_parser.GetVariable("_OUTER_P_QUADLION", OUTER_P_QUADLION);
//		_parser.GetVariable("_OUTER_D_QUADLION", OUTER_D_QUADLION);

		Fxy.Reset(5, (double *)_Fxy, TRUE);

		FzSim.Reset(3, (double *)_FzSim, TRUE);
		Fz.Reset(5, (double *)_Fz, TRUE);

		Fc.Reset(3, (double *)_Fc, TRUE);
		_parser.GetVariable("_Fxy", Fxy);
		_parser.GetVariable("_Fz", Fz);
		_parser.GetVariable("_FzSim", FzSim);
		_parser.GetVariable("_Fc", Fc);

		_parser.GetVariable("dc_ail2phi", &dc_ail2phi);
		_parser.GetVariable("dc_ele2tht", &dc_ele2tht);
		_parser.GetVariable("dc_thr2w", &dc_thr2w);
		_parser.GetVariable("dc_rud2r", &dc_rud2r);
		_parser.GetVariable("damping_u", &damping_u);
		_parser.GetVariable("damping_v", &damping_v);
		_parser.GetVariable("ratio_u", &ratio_u);
		_parser.GetVariable("ratio_v", &ratio_v);

		B5_vax[0] = B5_vax[1] = B5_vay[0] = B5_vay[1] = B5_vaz[0] = B5_vaz[1] = B5_vac = 0;
		m_bAutoPath = TRUE;
		m_bIntegrator = FALSE;
		m_bvisionFormation = m_bvisionFormationCloseloop = FALSE;
		m_bAdjust = FALSE;
		B5_t4 = -1;
		m_ctlPan = 0;
		m_ctlTilt = 0;

		m_UAVcontrol.Accgxc = m_UAVcontrol.Accgyc = m_UAVcontrol.Wc = m_UAVcontrol.dpsic = 0;

		double DCgain[4][4]      =
		{{1.6257, 0, 0, 0},
		{0, 1.6257, 0, 0},
		{0, 0, 0.29656, 0},
		{0, 0, 0, -0.1738}};

		m_DCgain = Mat(4, 4, CV_64F, DCgain);

		m_bTakeoff = m_bGPSPath = m_bReturnHome = m_bHFLY = m_bHold = m_bLand = m_bSemiAuto = m_bWaypoints = FALSE;
		m_bSweepPattern = m_bSmoothSweep = FALSE;
		m_bReflexxesInit = TRUE;
		m_bSemiAutoInit = TRUE;
		m_bNoGPS = m_bImageLog = FALSE;
		m_bInAir = FALSE;
		m_bSyncPath = FALSE;
		m_bSyncStart = FALSE;
		m_bTakeoffAdjust = m_bDisarmFlag = FALSE;
		m_cLeader = 3.1415926;
	    // Creating all relevant objects of the Type IV Reflexxes Motion Library

		double maxvel[4] = {0};
		double maxacc[4] = {0};
		double maxjerk[4] = {0};

		_parser.GetVariable("reflexxes_max_vel_xy", &maxvel[0]);
		_parser.GetVariable("reflexxes_max_vel_xy", &maxvel[1]);
		_parser.GetVariable("reflexxes_max_vel_z", &maxvel[2]);
		_parser.GetVariable("reflexxes_max_vel_c", &maxvel[3]);

		_parser.GetVariable("reflexxes_max_acc_xy", &maxacc[0]);
		_parser.GetVariable("reflexxes_max_acc_xy", &maxacc[1]);
		_parser.GetVariable("reflexxes_max_acc_z", &maxacc[2]);
		_parser.GetVariable("reflexxes_max_acc_c", &maxacc[3]);

		_parser.GetVariable("reflexxes_max_jerk_xy", &maxjerk[0]);
		_parser.GetVariable("reflexxes_max_jerk_xy", &maxjerk[1]);
		_parser.GetVariable("reflexxes_max_jerk_z", &maxjerk[2]);
		_parser.GetVariable("reflexxes_max_jerk_c", &maxjerk[3]);

		_parser.GetVariable("sweep_velocity", &m_sweepVel);
		_parser.GetVariable("sweep_distance", &m_sweepDist);

		_parser.GetVariable("takeoff_offsetX", &m_takeOffsetX);
		_parser.GetVariable("takeoff_offsetY", &m_takeOffsetY);

		ResetReflexxesCallFlag();
	    RML	=	new ReflexxesAPI(NUMBER_OF_DOFS, CYCLE_TIME_IN_SECONDS);
	    IP	=	new RMLPositionInputParameters(NUMBER_OF_DOFS);
	    OP	=	new RMLPositionOutputParameters(NUMBER_OF_DOFS);

	    IP->MaxVelocityVector->VecData			[0]	=	 maxvel[0]; //3.0		;
	    IP->MaxVelocityVector->VecData			[1]	=	 maxvel[1]; //3.0		;
	    IP->MaxVelocityVector->VecData			[2]	=	 maxvel[2]; //1.5		;
	    IP->MaxVelocityVector->VecData			[3]	=	 maxvel[3];

	    IP->MaxAccelerationVector->VecData		[0]	=	 maxacc[0]		;
	    IP->MaxAccelerationVector->VecData		[1]	=	 maxacc[1]		;
	    IP->MaxAccelerationVector->VecData		[2]	=	 maxacc[2]		;
	    IP->MaxAccelerationVector->VecData		[3]	=	 maxacc[3]		;

	    IP->MaxJerkVector->VecData				[0]	=	 maxjerk[0]		;
	    IP->MaxJerkVector->VecData				[1]	=	 maxjerk[1]		;
	    IP->MaxJerkVector->VecData				[2]	=	 maxjerk[2]		;
	    IP->MaxJerkVector->VecData				[3]	=	 maxjerk[3]		;

	    IP->SelectionVector->VecData[0]	=	true;
	    IP->SelectionVector->VecData[1]	=	true;
	    IP->SelectionVector->VecData[2]	=	true;
	    IP->SelectionVector->VecData[3]	=	true;

	    // initialize reflexxes variables in velocity mode
	    m_velRML =	new ReflexxesAPI(NUMBER_OF_DOFS, CYCLE_TIME_IN_SECONDS);
	    m_velIP	 =	new RMLVelocityInputParameters(NUMBER_OF_DOFS);
	    m_velOP	 =	new RMLVelocityOutputParameters(NUMBER_OF_DOFS);

	    m_velIP->MaxAccelerationVector->VecData	    [0]	=	 maxacc[0]		;
	    m_velIP->MaxAccelerationVector->VecData		[1]	=	 maxacc[1]		;
	    m_velIP->MaxAccelerationVector->VecData		[2]	=	 maxacc[2]		;
	    m_velIP->MaxAccelerationVector->VecData		[3]	=	 maxacc[3]		;

	    m_velIP->MaxJerkVector->VecData				[0]	=	 maxjerk[0]		;
	    m_velIP->MaxJerkVector->VecData				[1]	=	 maxjerk[1]		;
	    m_velIP->MaxJerkVector->VecData				[2]	=	 maxjerk[2]		;
	    m_velIP->MaxJerkVector->VecData				[3]	=	 maxjerk[3]		;

	    m_velIP->SelectionVector->VecData[0]	=	true;
	    m_velIP->SelectionVector->VecData[1]	=	true;
	    m_velIP->SelectionVector->VecData[2]	=	true;
	    m_velIP->SelectionVector->VecData[3]	=	true;

	    m_velFlags.SynchronizationBehavior = RMLFlags::ONLY_TIME_SYNCHRONIZATION;
	}

	B5_adjustc = 0;
	B5_t1 = -1;
	B5_t2 = -1;
	B5_t3 = -1;
}

clsPlan::clsPlan()
{
	m_behavior.behavior = 0;
}

clsPlan::~clsPlan()
{
}

void clsCTL::SetPlan(int nPlan){
	if (nPlan == 6) { m_pPlan = &m_planFullTask; SetFlag_FULL_PLAN();}
}



void clsFullTaskPlan::Reset()
{
	m_mode = READY;
	printf("Full task plan reset\n");
}

int clsFullTaskPlan::Run()
{
	EVENT &event = _state.GetEvent();
	UAVSTATE &state = _state.GetState();
	m_behavior.behavior = 0;

	switch (m_mode) {
	case READY:
		m_mode = ENGINEUP;
		m_behavior.behavior = BEHAVIOR_ENGINEUP;
		break;

	case ENGINEUP:
		if (event.code == EVENT_BEHAVIOREND && (int &)event.info[0] == BEHAVIOR_ENGINEUP) {
			_state.ClearEvent();
			m_mode = ASCEND;

			m_behavior.behavior = BEHAVIOR_TAKEOFF;

			// set take off height
			double takeoffHeight;
			_parser.GetVariable("takeoff_height", &takeoffHeight);
			(double &)m_behavior.parameter[0] = takeoffHeight;//_ctl.GetTakeoffHeight();

		}
		break;

	case ASCEND:
		if ( (event.code == EVENT_BEHAVIOREND && (int &)event.info[0] == BEHAVIOR_TAKEOFF) ) {
			_state.ClearEvent();
			m_mode = HOLD;
			m_behavior.behavior = BEHAVIOR_HOLD;
			m_tHoldStart = ::GetTime();
			(double &)m_behavior.parameter[0] = _ctl.B5_pnr[0]; // state.x;
			(double &)m_behavior.parameter[8] = _ctl.B5_pnr[1]; // state.y;
			(double &)m_behavior.parameter[16] = _ctl.B5_pnr[2]; // state.z;
			(double &)m_behavior.parameter[24] = _ctl.B5_pnr[3]; // state.c;
		}
		break;

	case HOLD:
		if (::GetTime() - m_tHoldStart > 5) {
			printf("Navigate mode\n");
			m_mode = NAVIGATE;
			m_behavior.behavior = BEHAVIOR_TEST;
			printf("[Plan full] ENTER Test!\n");
			if(INDOOR_OUTDOOR_FLAG == 1)
				(int &)m_behavior.parameter[0] = 18;
			else
				(int &)m_behavior.parameter[0] = 20;
		}
		break;

	case NAVIGATE:
		if ( (event.code == EVENT_BEHAVIOREND && (int &)event.info[0] == BEHAVIOR_TEST) )
		{
			m_mode = LAND;
		  	m_behavior.behavior = BEHAVIOR_LAND;
		}
		break;
	}
	return m_mode != LAND;
}



void clsCTL::ReflexxesPathPlanning(UAVSTATE state, QROTOR_REF ref, double &syncTime, double pnr[4], double vnr[3], double anr[3])
{

	if (_svo.GetCTLMode() == CTL_MODE_TRANSIT_SEMI2AUTO) {
		_ctl.B5_t1 = ::GetTime();
		printf("Reflexxes reset with semiauto ref  ===================== \n");
	    IP->CurrentPositionVector->VecData		[0]	= B5_pnr[0];
	    IP->CurrentPositionVector->VecData		[1]	= B5_pnr[1];
	    IP->CurrentPositionVector->VecData		[2]	= B5_pnr[2];
	    IP->CurrentPositionVector->VecData		[3]	= B5_pnr[3];

	    IP->CurrentVelocityVector->VecData		[0]	= B5_vnr[0];
	    IP->CurrentVelocityVector->VecData		[1]	= B5_vnr[1];
	    IP->CurrentVelocityVector->VecData		[2]	= B5_vnr[2];
	    IP->CurrentVelocityVector->VecData		[3]	= B5_vnr[3];

	    IP->CurrentAccelerationVector->VecData	[0]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[1]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[2]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[3]	= 0.0		;
	}
	else if ((_svo.GetCTLMode() == CTL_MODE_TRANSIT_MANUAL2AUTO || _ctl.B5_t1 < 0)) {
		_ctl.B5_t1 = ::GetTime();
		printf("Reflexxes reset with state  ===================== \n");
	    IP->CurrentPositionVector->VecData		[0]	= state.x;
	    IP->CurrentPositionVector->VecData		[1]	= state.y;
	    IP->CurrentPositionVector->VecData		[2]	= state.z;
	    IP->CurrentPositionVector->VecData		[3]	= state.c;

	    IP->CurrentVelocityVector->VecData		[0]	= state.ug	;
	    IP->CurrentVelocityVector->VecData		[1]	= state.vg	;
	    IP->CurrentVelocityVector->VecData		[2]	= state.wg	;
	    IP->CurrentVelocityVector->VecData		[3]	= state.r	;

	    IP->CurrentAccelerationVector->VecData	[0]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[1]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[2]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[3]	= 0.0		;
	}

	IP->TargetPositionVector->VecData		[0]	=	ref.p_x_r		;
	IP->TargetPositionVector->VecData		[1]	=	ref.p_y_r		;
	IP->TargetPositionVector->VecData		[2]	=	ref.p_z_r		;
	IP->TargetPositionVector->VecData		[3]	=	ref.psi_r		;

	IP->TargetVelocityVector->VecData		[0]	= ref.v_x_r		;
	IP->TargetVelocityVector->VecData		[1]	= ref.v_y_r		;
	IP->TargetVelocityVector->VecData		[2]	= ref.v_z_r		;
	IP->TargetVelocityVector->VecData		[3]	= 0.0		;


    // Calling the Reflexxes OTG algorithm
    int ResultValue	= RML->RMLPosition(*IP, OP, Flags);
    if (ResultValue < 0)
    {
        printf("An error occurred (%d).\n", ResultValue	);
        printf("%s\n", OP->GetErrorString());
    }

//    printf("New position/pose vector                  : ");
    for (int j = 0; j < NUMBER_OF_DOFS; j++)
    {
//        printf("%10.3lf ", OP->NewPositionVector->VecData[j]);
        pnr[j] = OP->NewPositionVector->VecData[j];
    }
//    printf("\n");
//    printf("New velocity vector                       : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewVelocityVector->VecData[j]);
        vnr[j] = OP->NewVelocityVector->VecData[j];
    }
//    printf("\n");
//    printf("New acceleration vector                   : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewAccelerationVector->VecData[j]);
        anr[j] = OP->NewAccelerationVector->VecData[j];
    }

    // Feed the output values of the current control cycle back to
    // input values of the next control cycle
    syncTime = OP->SynchronizationTime;
    *IP->CurrentPositionVector		=	*OP->NewPositionVector		;
    *IP->CurrentVelocityVector		=	*OP->NewVelocityVector		;
    *IP->CurrentAccelerationVector	=	*OP->NewAccelerationVector	;

}

void clsCTL::ReflexxesPathPlanning(UAVSTATE state, QROTOR_REF ref, double pnr[4], double vnr[3], double anr[3])
{
/*	if (FALSE GetReflexxesInitFlag()) {
		IP->TargetPositionVector->VecData		[0]	=	B5_pnr[0]		;
		IP->TargetPositionVector->VecData		[1]	=	B5_pnr[1]		;
		IP->TargetPositionVector->VecData		[2]	=	B5_pnr[2]		;
		IP->TargetPositionVector->VecData		[3]	=	B5_pnr[3]		;

		IP->TargetVelocityVector->VecData		[0]	= B5_vnr[0]		;
		IP->TargetVelocityVector->VecData		[1]	= B5_vnr[1]		;
		IP->TargetVelocityVector->VecData		[2]	= B5_vnr[2]		;
		IP->TargetVelocityVector->VecData		[3]	= 0.0		;

		ResetReflexxesInitFlag();
	}*/
	if (_ctl.B5_t1 < 0) {
		_ctl.B5_t1 = ::GetTime();
		printf("Reflexxes reset\n");
	    IP->CurrentPositionVector->VecData		[0]	= state.x;
	    IP->CurrentPositionVector->VecData		[1]	= state.y;
	    IP->CurrentPositionVector->VecData		[2]	= state.z;
	    IP->CurrentPositionVector->VecData		[3]	= state.c;

	    IP->CurrentVelocityVector->VecData		[0]	= state.ug	;
	    IP->CurrentVelocityVector->VecData		[1]	= state.vg	;
	    IP->CurrentVelocityVector->VecData		[2]	= state.wg	;
	    IP->CurrentVelocityVector->VecData		[3]	= state.r	;

	    IP->CurrentAccelerationVector->VecData	[0]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[1]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[2]	= 0.0		;
	    IP->CurrentAccelerationVector->VecData	[3]	= 0.0		;
	}
//	else {
		IP->TargetPositionVector->VecData		[0]	=	ref.p_x_r		;
		IP->TargetPositionVector->VecData		[1]	=	ref.p_y_r		;
		IP->TargetPositionVector->VecData		[2]	=	ref.p_z_r		;
		IP->TargetPositionVector->VecData		[3]	=	ref.psi_r		;

		IP->TargetVelocityVector->VecData		[0]	= ref.v_x_r		;
		IP->TargetVelocityVector->VecData		[1]	= ref.v_y_r		;
		IP->TargetVelocityVector->VecData		[2]	= ref.v_z_r		;
		IP->TargetVelocityVector->VecData		[3]	= 0.0		;
//	}

    // Calling the Reflexxes OTG algorithm
    int ResultValue	= RML->RMLPosition(*IP, OP, Flags);
    if (ResultValue < 0)
    {
        printf("An error occurred (%d).\n", ResultValue	);
        printf("%s\n", OP->GetErrorString());
    }

//    printf("New position/pose vector                  : ");
    for (int j = 0; j < NUMBER_OF_DOFS; j++)
    {
//        printf("%10.3lf ", OP->NewPositionVector->VecData[j]);
        pnr[j] = OP->NewPositionVector->VecData[j];
    }
//    printf("\n");
//    printf("New velocity vector                       : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewVelocityVector->VecData[j]);
        vnr[j] = OP->NewVelocityVector->VecData[j];
    }
//    printf("\n");
//    printf("New acceleration vector                   : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewAccelerationVector->VecData[j]);
        anr[j] = OP->NewAccelerationVector->VecData[j];
    }

    // Feed the output values of the current control cycle back to
    // input values of the next control cycle
    *IP->CurrentPositionVector		=	*OP->NewPositionVector		;
    *IP->CurrentVelocityVector		=	*OP->NewVelocityVector		;
    *IP->CurrentAccelerationVector	=	*OP->NewAccelerationVector	;
}

void clsCTL::ReflexxesPathPlanningVelocityMode(UAVSTATE state, QROTOR_REF ref, double pnr[4], double vnr[4], double anr[3])
{
	if (_svo.GetCTLMode() == CTL_MODE_TRANSIT_AUTO2SEMI) {
		_ctl.B5_t1 = ::GetTime();
		printf("Reflexxes velocity mode reset with auto ref ******************** \n");
	    m_velIP->CurrentPositionVector->VecData		[0]	= B5_pnr[0];
	    m_velIP->CurrentPositionVector->VecData		[1]	= B5_pnr[1];
	    m_velIP->CurrentPositionVector->VecData		[2]	= B5_pnr[2];
	    m_velIP->CurrentPositionVector->VecData		[3]	= B5_pnr[3];

	    m_velIP->CurrentVelocityVector->VecData		[0]	= B5_vnr[0];
	    m_velIP->CurrentVelocityVector->VecData		[1]	= B5_vnr[1];
	    m_velIP->CurrentVelocityVector->VecData		[2]	= B5_vnr[2];
	    m_velIP->CurrentVelocityVector->VecData		[3]	= B5_vnr[3];

	    m_velIP->CurrentAccelerationVector->VecData	[0]	= 0.0		;
	    m_velIP->CurrentAccelerationVector->VecData	[1]	= 0.0		;
	    m_velIP->CurrentAccelerationVector->VecData	[2]	= 0.0		;
	    m_velIP->CurrentAccelerationVector->VecData	[3]	= 0.0		;
	    ResetSemiAutoInitFlag();
	}
	else if (_svo.GetCTLMode() == CTL_MODE_TRANSIT_MANUAL2SEMI || GetSemiAutoInitFlag() || _ctl.B5_t1 < 0) {
		_ctl.B5_t1 = ::GetTime();
		printf("Reflexxes velocity mode reset with state ******************** \n");
	    m_velIP->CurrentPositionVector->VecData		[0]	= state.x;
	    m_velIP->CurrentPositionVector->VecData		[1]	= state.y;
	    m_velIP->CurrentPositionVector->VecData		[2]	= state.z;
	    m_velIP->CurrentPositionVector->VecData		[3]	= state.c;

	    m_velIP->CurrentVelocityVector->VecData		[0]	= state.ug	;
	    m_velIP->CurrentVelocityVector->VecData		[1]	= state.vg	;
	    m_velIP->CurrentVelocityVector->VecData		[2]	= state.wg	;
	    m_velIP->CurrentVelocityVector->VecData		[3]	= state.r	;

	    m_velIP->CurrentAccelerationVector->VecData	[0]	= 0.0		;
	    m_velIP->CurrentAccelerationVector->VecData	[1]	= 0.0		;
	    m_velIP->CurrentAccelerationVector->VecData	[2]	= 0.0		;
	    m_velIP->CurrentAccelerationVector->VecData	[3]	= 0.0		;
	    ResetSemiAutoInitFlag();
	}

	m_velIP->TargetVelocityVector->VecData		[0]	= ref.v_x_r		;
	m_velIP->TargetVelocityVector->VecData		[1]	= ref.v_y_r		;
	m_velIP->TargetVelocityVector->VecData		[2]	= ref.v_z_r		;
	m_velIP->TargetVelocityVector->VecData		[3]	= ref.r_r		;

	// call the reflexxes API to generate references
	int ResultValue	= m_velRML->RMLVelocity(*m_velIP, m_velOP, m_velFlags);
    if (ResultValue < 0)
    {
        printf("An error occurred (%d).\n", ResultValue	);
        printf("%s\n", m_velOP->GetErrorString());
    }

    for (int j = 0; j < NUMBER_OF_DOFS; j++)
    {
//        printf("%10.3lf ", OP->NewPositionVector->VecData[j]);
        pnr[j] = m_velOP->NewPositionVector->VecData[j];
    }
//    printf("\n");
    for (int j = 0; j < NUMBER_OF_DOFS; j++)
    {
//        printf("%10.3lf ", OP->NewVelocityVector->VecData[j]);
        vnr[j] = m_velOP->NewVelocityVector->VecData[j];
    }
//    printf("\n");
//    printf("New acceleration vector                   : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewAccelerationVector->VecData[j]);
        anr[j] = m_velOP->NewAccelerationVector->VecData[j];
    }

    // Feed the output values of the current control cycle back to
    // input values of the next control cycle
    *m_velIP->CurrentPositionVector      =   *m_velOP->NewPositionVector      ;
    *m_velIP->CurrentVelocityVector      =   *m_velOP->NewVelocityVector      ;
    *m_velIP->CurrentAccelerationVector  =   *m_velOP->NewAccelerationVector  ;

}

void clsCTL::ReflexxesPathPlanning(QROTOR_REF ref, double pnr[4], double vnr[3], double anr[3])
{
	if (FALSE) {
		IP->TargetPositionVector->VecData		[0]	=	B5_pnr[0]		;
		IP->TargetPositionVector->VecData		[1]	=	B5_pnr[1]		;
		IP->TargetPositionVector->VecData		[2]	=	B5_pnr[2]		;
		IP->TargetPositionVector->VecData		[3]	=	B5_pnr[3]		;

		IP->TargetVelocityVector->VecData		[0]	= B5_vnr[0]		;
		IP->TargetVelocityVector->VecData		[1]	= B5_vnr[1]		;
		IP->TargetVelocityVector->VecData		[2]	= B5_vnr[2]		;
		IP->TargetVelocityVector->VecData		[3]	= 0.0		;

		ResetReflexxesInitFlag();
	}

	else {
		IP->TargetPositionVector->VecData		[0]	=	ref.p_x_r		;
		IP->TargetPositionVector->VecData		[1]	=	ref.p_y_r		;
		IP->TargetPositionVector->VecData		[2]	=	ref.p_z_r		;
		IP->TargetPositionVector->VecData		[3]	=	ref.psi_r		;

		IP->TargetVelocityVector->VecData		[0]	= ref.v_x_r		;
		IP->TargetVelocityVector->VecData		[1]	= ref.v_y_r		;
		IP->TargetVelocityVector->VecData		[2]	= ref.v_z_r		;
		IP->TargetVelocityVector->VecData		[3]	= 0.0		;
	}

    // Calling the Reflexxes OTG algorithm
    int ResultValue	= RML->RMLPosition(*IP, OP, Flags);
    if (ResultValue < 0)
    {
        printf("An error occurred (%d).\n", ResultValue	);
        printf("%s\n", OP->GetErrorString());
    }

//    printf("New position/pose vector                  : ");
    for (int j = 0; j < NUMBER_OF_DOFS; j++)
    {
//        printf("%10.3lf ", OP->NewPositionVector->VecData[j]);
        pnr[j] = OP->NewPositionVector->VecData[j];
    }
//    printf("\n");
//    printf("New velocity vector                       : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewVelocityVector->VecData[j]);
        vnr[j] = OP->NewVelocityVector->VecData[j];
    }
//    printf("\n");
//    printf("New acceleration vector                   : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewAccelerationVector->VecData[j]);
        anr[j] = OP->NewAccelerationVector->VecData[j];
    }

    // Feed the output values of the current control cycle back to
    // input values of the next control cycle
    *IP->CurrentPositionVector		=	*OP->NewPositionVector		;
    *IP->CurrentVelocityVector		=	*OP->NewVelocityVector		;
    *IP->CurrentAccelerationVector	=	*OP->NewAccelerationVector	;

}

void clsCTL::ReflexxesPathPlanningChangeParas(QROTOR_REF ref, double pnr[4], double vnr[3], double anr[3])
{
//	if (GetReflexxesInitFlag()) {
	    IP->MaxVelocityVector->VecData			[0]	=	 1.0		;
	    IP->MaxVelocityVector->VecData			[1]	=	 1.0		;
	    IP->MaxVelocityVector->VecData			[2]	=	 0.5; //1.5		;
	    IP->MaxVelocityVector->VecData			[3]	=	 0.2		;

	    IP->MaxAccelerationVector->VecData		[0]	=	 0.2		;
	    IP->MaxAccelerationVector->VecData		[1]	=	 0.2		;
	    IP->MaxAccelerationVector->VecData		[2]	=	 0.2		;
	    IP->MaxAccelerationVector->VecData		[3]	=	 0.2		;

	    IP->MaxJerkVector->VecData				[0]	=	 0.2		;
	    IP->MaxJerkVector->VecData				[1]	=	 0.2		;
	    IP->MaxJerkVector->VecData				[2]	=	 0.2		;
	    IP->MaxJerkVector->VecData				[3]	=	 0.1		;

//		ResetReflexxesInitFlag();
//	}
//	else {
		IP->TargetPositionVector->VecData		[0]	=	ref.p_x_r		;
		IP->TargetPositionVector->VecData		[1]	=	ref.p_y_r		;
		IP->TargetPositionVector->VecData		[2]	=	ref.p_z_r		;
		IP->TargetPositionVector->VecData		[3]	=	ref.psi_r		;

		IP->TargetVelocityVector->VecData		[0]	= ref.v_x_r		;
		IP->TargetVelocityVector->VecData		[1]	= ref.v_y_r		;
		IP->TargetVelocityVector->VecData		[2]	= ref.v_z_r		;
		IP->TargetVelocityVector->VecData		[3]	= 0.0		;
//	}

    // Calling the Reflexxes OTG algorithm
    int ResultValue	= RML->RMLPosition(*IP, OP, Flags);
    if (ResultValue < 0)
    {
        printf("An error occurred (%d).\n", ResultValue	);
        printf("%s\n", OP->GetErrorString());
    }

//    printf("New position/pose vector                  : ");
    for (int j = 0; j < NUMBER_OF_DOFS; j++)
    {
//        printf("%10.3lf ", OP->NewPositionVector->VecData[j]);
        pnr[j] = OP->NewPositionVector->VecData[j];
    }
//    printf("\n");
//    printf("New velocity vector                       : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewVelocityVector->VecData[j]);
        vnr[j] = OP->NewVelocityVector->VecData[j];
    }
//    printf("\n");
//    printf("New acceleration vector                   : ");
    for (int j = 0; j < NUMBER_OF_DOFS - 1; j++)
    {
//        printf("%10.3lf ", OP->NewAccelerationVector->VecData[j]);
        anr[j] = OP->NewAccelerationVector->VecData[j];
    }

    // Feed the output values of the current control cycle back to
    // input values of the next control cycle
    *IP->CurrentPositionVector		=	*OP->NewPositionVector		;
    *IP->CurrentVelocityVector		=	*OP->NewVelocityVector		;
    *IP->CurrentAccelerationVector	=	*OP->NewAccelerationVector	;

}

void clsCTL::GetTakeoffRef() {
	double tStart = _ctl.GetPathStartTime();
	double tElapse = ::GetTime() - tStart;

	int nUAVStatus = _ctl.GetUAVStatus();
//	printf("Takeoff process, status %d\n", nUAVStatus);
	if (_svo.GetCurrentCTLMode() == CTL_MODE_SEMIAUTO && nUAVStatus == STATUS_AIR) {
		m_behavior.behavior = BEHAVIOR_SEMIAUTO;
		ResetTakeoffFlag();
		SetSemiAutoInitFlag();
		m_semiSig = m_sig;
		printf("-------- Set to semiauto mode once in the air ----------\n");
		_svo.ResetSemiOnGroundTimeFlag();
		return;
	}

	UAVSTATE state = _state.GetState();
	if ((tElapse > m_tTakeoff) && (state.z < B5_pnr[3])) {
		ResetTakeoffFlag();
//		printf("Take off finished\n");
		char msg[256];
		::sprintf(msg, "Behavior %d(%s) finished", m_behaviorCurrent.behavior, GetBehaviorString(m_behaviorCurrent.behavior));
		_cmm.PutMessage(msg);
		SetIntegratorFlag();
		_state.SetEvent(EVENT_BEHAVIOREND, m_behaviorCurrent.behavior);

		if (_svo.GetCurrentCTLMode() == CTL_MODE_SEMIAUTO && nUAVStatus == STATUS_AIR) {
			m_behavior.behavior = BEHAVIOR_SEMIAUTO;
			ResetTakeoffFlag();
			SetSemiAutoInitFlag();
			m_semiSig = m_sig;
			printf("-------- Set to semiauto mode once in the air2 ----------\n");
		}
		return;
	} else {
		ReflexxesPathPlanning(m_reflexRef, B5_pnr, B5_vnr, B5_anr);
	}

}

void clsCTL::GetLandRef() {
	double tStart = _ctl.GetPathStartTime();
	double tElapse = ::GetTime() - tStart;

	UAVSTATE &state = _state.GetState();
	// check height reference
	if ( B5_pnr[2] < -7.0 ) {
		IP->MaxVelocityVector->VecData[2]	=	 0.5; // change wg max during landing
		ReflexxesPathPlanning(state, m_reflexRef, B5_pnr, B5_vnr, B5_anr);
	} else {
		m_reflexRef.v_z_r = 0.3;
		m_reflexRef.p_z_r = 50;
		ReflexxesPathPlanningChangeParas(m_reflexRef, B5_pnr, B5_vnr, B5_anr);
	}

	if (tElapse > m_tLand + 500) {
		ResetLandFlag();
		printf("Land finished\n");
		return;
	}
}

void clsCTL::GetReturnHomeRef() {
	double tStart = _ctl.GetPathStartTime();
	double tElapse = ::GetTime() - tStart;

	if (tElapse > m_tReturnHome) {
		ResetReturnHomeFlag();
		_state.SetEvent(EVENT_BEHAVIOREND, BEHAVIOR_RETURNHOME);
//		printf("Return home path finished\n");
		char msg[256];
		::sprintf(msg, "Behavior %d(%s) finished", m_behaviorCurrent.behavior, GetBehaviorString(m_behaviorCurrent.behavior));
		_cmm.PutMessage(msg);
		return;
	} else {
		ReflexxesPathPlanning(m_reflexRef, B5_pnr, B5_vnr, B5_anr);
	}
}

void clsCTL::GetHFLYRef() {
	double tStart = _ctl.GetPathStartTime();
	double tElapse = ::GetTime() - tStart;

	if (tElapse > m_tHFLY) {
		ResetHFLYFlag();
		_state.SetEvent(EVENT_BEHAVIOREND, BEHAVIOR_HFLY);
//		printf("HFLY path finished\n");
		char msg[256];
		::sprintf(msg, "Behavior %d(%s) finished", m_behaviorCurrent.behavior, GetBehaviorString(m_behaviorCurrent.behavior));
		_cmm.PutMessage(msg);
		return;
	} else {
		ReflexxesPathPlanning(m_reflexRef, B5_pnr, B5_vnr, B5_anr);
	}
}

void clsCTL::GetSemiAutoRef() {
	UAVSTATE &state = _state.GetState();
	SVODATA &svodata = _svo.GetSVOData();
/*	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[CTL] PIXHAWK manual data, ea %.3f, ee %.3f, et %.3f, er %.3f\n",
				svodata.aileron, svodata.elevator, svodata.throttle, svodata.rudder);
	}*/
	// transform velocity reference from local heading frame to local NED frame
	double vhr[4] = {svodata.elevator * REFLEXXES_MAX_VEL_X, svodata.aileron * REFLEXXES_MAX_VEL_Y,
			-svodata.throttle * REFLEXXES_MAX_VEL_Z, svodata.rudder * REFLEXXES_MAX_VEL_C};

/*	if (m_nCount % 50 == 0) {
		printf("[CTL] semi auto vel ref: ail %.3f, ele %.3f, thr %.3f, rud %.3f \n",
				vhr[0], vhr[1], vhr[2], vhr[3]);
	}*/
	double vnr[3] = {0};
	double c0 = state.c;
	double abcg[3] = {0, 0, c0};
	B2G(abcg, vhr, vnr);
	range(vnr[0], -REFLEXXES_MAX_VEL_X, REFLEXXES_MAX_VEL_X);
	range(vnr[1], -REFLEXXES_MAX_VEL_Y, REFLEXXES_MAX_VEL_Y);
	range(vnr[2], -REFLEXXES_MAX_VEL_Z+0.2, REFLEXXES_MAX_VEL_Z-0.2);

	if (m_nCount % 50 == 0) {
		printf("[CTL] vnr: %.3f, %.3f, %.3f \n",
				vnr[0], vnr[1], vnr[2]);
	}

	m_semipnr[0] += vnr[0]*0.02; m_semipnr[1] += vnr[1]*0.02; m_semipnr[2] += vnr[2]*0.02;
	m_semipnr[3] += vhr[3]*0.02;
	m_reflexRef.p_x_r = m_semipnr[0]; m_reflexRef.p_y_r = m_semipnr[1]; m_reflexRef.p_z_r = m_semipnr[2];
	m_reflexRef.psi_r = m_semipnr[3];
	m_reflexRef.v_x_r = vnr[0]; m_reflexRef.v_y_r = vnr[1]; m_reflexRef.v_z_r = vnr[2];

	IP->MaxVelocityVector->VecData[0] = 10.0;
	IP->MaxVelocityVector->VecData[1] = 10.0;
	IP->MaxVelocityVector->VecData[2] = 5.0;

    IP->MaxAccelerationVector->VecData		[0]	=	 1.2; //REFLEXXES_MAX_ACC_X		;
    IP->MaxAccelerationVector->VecData		[1]	=	 1.2; //REFLEXXES_MAX_ACC_Y		;
    IP->MaxAccelerationVector->VecData		[2]	=	 1.2; //REFLEXXES_MAX_ACC_Z		;
    IP->MaxAccelerationVector->VecData		[3]	=	 1.0; //REFLEXXES_MAX_ACC_C		;

    IP->MaxJerkVector->VecData				[0]	=	 2.0; //5.0		;
    IP->MaxJerkVector->VecData				[1]	=	 2.0; //5.0		;
    IP->MaxJerkVector->VecData				[2]	=	 2.0; //5.0		;
    IP->MaxJerkVector->VecData				[3]	=	 1.0		;

	ReflexxesPathPlanning(state, m_reflexRef, B5_pnr, B5_vnr, B5_anr);

}

void clsCTL::GetSemiAutoRefInVelocityMode() {
	UAVSTATE &state = _state.GetState();
	SVODATA &svodata = _svo.GetSVOData();
/*	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[CTL] PIXHAWK manual data, ea %.3f, ee %.3f, et %.3f, er %.3f\n",
				svodata.aileron, svodata.elevator, svodata.throttle, svodata.rudder);
	}*/
	// transform velocity reference from local heading frame to local NED frame
	double maxVelX =  IP->MaxVelocityVector->VecData	    [0];
	double maxVelY =  IP->MaxVelocityVector->VecData	    [1];
	double maxVelZ =  IP->MaxVelocityVector->VecData	    [2];
	double maxVelC =  IP->MaxVelocityVector->VecData	    [3];
	double vhr[4] = {svodata.elevator * maxVelX, svodata.aileron * maxVelY,
			-svodata.throttle * maxVelZ, svodata.rudder * maxVelC};

	double vnr[3] = {0};
	double c0 = state.c;
	double abcg[3] = {0, 0, c0};
	B2G(abcg, vhr, vnr);
	range(vnr[0], -REFLEXXES_MAX_VEL_X, REFLEXXES_MAX_VEL_X);
	range(vnr[1], -REFLEXXES_MAX_VEL_Y, REFLEXXES_MAX_VEL_Y);
	range(vnr[2], -REFLEXXES_MAX_VEL_Z+0.2, REFLEXXES_MAX_VEL_Z-0.2);

	if (m_nCount % 50 == 0) {
		printf("[CTL] vnr: %.3f, %.3f, %.3f \n",
				vnr[0], vnr[1], vnr[2]);
	}

	m_reflexRef.p_x_r = 0; m_reflexRef.p_y_r = 0; m_reflexRef.p_z_r = 0; m_reflexRef.psi_r = 0;
	m_reflexRef.v_x_r = vnr[0]; m_reflexRef.v_y_r = vnr[1]; m_reflexRef.v_z_r = vnr[2];
	m_reflexRef.r_r = vhr[3];
	ReflexxesPathPlanningVelocityMode(state, m_reflexRef, B5_pnr, B5_vnr, B5_anr);
}




void clsCTL::SetTakeoffPos(double x, double y) {
	m_takeoffPos[0] = x;
	m_takeoffPos[1] = y;
}

void clsCTL::GetTakeoffPos(double &x, double &y) {
	x = m_takeoffPos[0]; y = m_takeoffPos[1];
}

void clsCTL::ProcessCtlMode() {
	int nMode = _svo.GetCTLMode();
	int nCurMode = _svo.GetCurrentCTLMode();
	int nUAVStatus = _ctl.GetUAVStatus();

	switch(nMode) {
	case CTL_MODE_TRANSIT_SEMI2MANUAL:
	case CTL_MODE_TRANSIT_AUTO2MANUAL:
		// can be switched from any status either in air or ground
		m_fControl = CONTROLFLAG_MANUAL_INNERLOOP;
		printf("2MANUAL\n");
		break;

	case CTL_MODE_TRANSIT_MANUAL2AUTO:
	case CTL_MODE_TRANSIT_SEMI2AUTO:
		// can be switched from any status either in air or ground
		// if on ground, then only "engine" auto behavior can be triggerred
		if (nUAVStatus == STATUS_AIR)
			m_behavior.behavior = BEHAVIOR_HFLY;
		printf("2AUTO\n");
		break;

	case CTL_MODE_TRANSIT_MANUAL2SEMI:
	case CTL_MODE_TRANSIT_AUTO2SEMI:
		// only valid in air, cannot permit when on ground
		if (nUAVStatus == STATUS_AIR) {
			ResetHFLYFlag();
			m_behavior.behavior = BEHAVIOR_SEMIAUTO;
			printf("2SEMI in air\n");
		}
		break;

	default:
		break;
	}

	if (nCurMode == CTL_MODE_SEMIAUTO) {
		if (nUAVStatus == STATUS_GROUND) {
			// if throttle position is above middle for 2s, then start auto takeoff behavior to 1.5 m height
//			static BOOL bEnd = TRUE;
			if (_svo.GetSemiOnGroundTimeFlag()) {
				printf("Takeoff on ground with SEMI mode\n");
				SetTakeoffHeight(15);
				_ctl.SetPlan(2);
				m_pPlan->SetPlanID(2);
//				_svo.ResetSemiOnGroundTimeFlag();
//				bEnd = FALSE;
			}
		}
	}
}

int clsCTL::GetUAVStatus()
{
	double thtSig = _state.GetSIG().throttle;
	double z = _state.GetState().z;
	int nMode = _svo.GetCurrentCTLMode();

	static BOOL bAir1st = TRUE;
	if (z < Z_IN_AIR && thtSig > THROTTLE_SIG_INAIR && bAir1st) {
		SetInAIRFlag();
		bAir1st = FALSE;
		printf("STATUS_AIR\n");
		return STATUS_AIR;
	}
	else if (!GetInAirFlag()) {
/*		if (m_nCount % 50 == 0) {
			printf("STATUS_GROUND\n");
		}*/
		return STATUS_GROUND;
	}
	else if ( (nMode == CTL_MODE_AUTO && thtSig < THROTTLE_SIG_INAIR) || (nMode == CTL_MODE_SEMIAUTO && thtSig < THROTTLE_SIG_INAIR)
			|| (nMode == CTL_MODE_MANUAL && _svo.GetSVOData().throttle < -0.3) ) {
			ResetInAirFlag();
			bAir1st = TRUE;
//			if (m_nCount % 50 == 0) {
				printf("AIR to GROUND, thtSig %.3f\n", thtSig);
				SetDisarmFlag();
//			}
			// reset autoSig and semiSig to zero when touch ground from air
			::memset(&m_sig, 0, sizeof(m_sig));
			::memset(&m_semiSig, 0, sizeof(m_semiSig));
			::memset(B5_pnr, 0, 4 * sizeof(double));
			::memset(B5_vnr, 0, 4 * sizeof(double));
			::memset(B5_anr, 0, 3 * sizeof(double));
			B5_t1 = -1;
			return STATUS_GROUND;
	}
	else if (GetInAirFlag())
		return STATUS_AIR;
}

BOOL clsCTL::GetTrackingOKFlag()
{
	UAVSTATE &state = _state.GetState();
	double errX = state.x - B5_pnr[0];
	double errY = state.y - B5_pnr[1];
	double errZ = state.z - B5_pnr[2];

	static int counter = 0;
	if ( (errX*errX + errY*errY + errZ*errZ) >= 300 ) {
		counter++;

		if ( counter>10 ) return FALSE;
		else return TRUE;
	}
	else return TRUE;
//	return (errX*errX + errY*errY + errZ*errZ) < 300 ? TRUE: FALSE;
}
