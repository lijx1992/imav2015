// MESSAGE gumstix_2_pxHawk PACKING

#define MAVLINK_MSG_ID_gumstix_2_pxHawk 150

typedef struct __mavlink_gumstix_2_pxhawk_t
{
 int64_t time_usec; ///< Gumstix onboard time in usec 
 int32_t lon; ///< Gumstix GPS Longitude in degrees * 1E7
 int32_t lat; ///< Gumstix GPS Latitude in degrees * 1E7
 int32_t alt; ///< Gumstix GPS Altitude in degrees * 1E7
 float eulerAngle[3]; ///< Gumstix Euler Angle (roll, pitch, yaw) in degrees
 float angularRate[3]; ///< Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 float controlSignal[5]; ///< Gumstix Control Signals (aileron, elevator, pitch, yaw, throttle
} mavlink_gumstix_2_pxhawk_t;

#define MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN 64
#define MAVLINK_MSG_ID_150_LEN 64

#define MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC 204
#define MAVLINK_MSG_ID_150_CRC 204

#define MAVLINK_MSG_gumstix_2_pxHawk_FIELD_EULERANGLE_LEN 3
#define MAVLINK_MSG_gumstix_2_pxHawk_FIELD_ANGULARRATE_LEN 3
#define MAVLINK_MSG_gumstix_2_pxHawk_FIELD_CONTROLSIGNAL_LEN 5

#define MAVLINK_MESSAGE_INFO_gumstix_2_pxHawk { \
	"gumstix_2_pxHawk", \
	7, \
	{  { "time_usec", NULL, MAVLINK_TYPE_INT64_T, 0, 0, offsetof(mavlink_gumstix_2_pxhawk_t, time_usec) }, \
         { "lon", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_gumstix_2_pxhawk_t, lon) }, \
         { "lat", NULL, MAVLINK_TYPE_INT32_T, 0, 12, offsetof(mavlink_gumstix_2_pxhawk_t, lat) }, \
         { "alt", NULL, MAVLINK_TYPE_INT32_T, 0, 16, offsetof(mavlink_gumstix_2_pxhawk_t, alt) }, \
         { "eulerAngle", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_gumstix_2_pxhawk_t, eulerAngle) }, \
         { "angularRate", NULL, MAVLINK_TYPE_FLOAT, 3, 32, offsetof(mavlink_gumstix_2_pxhawk_t, angularRate) }, \
         { "controlSignal", NULL, MAVLINK_TYPE_FLOAT, 5, 44, offsetof(mavlink_gumstix_2_pxhawk_t, controlSignal) }, \
         } \
}


/**
 * @brief Pack a gumstix_2_pxhawk message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_usec Gumstix onboard time in usec 
 * @param lon Gumstix GPS Longitude in degrees * 1E7
 * @param lat Gumstix GPS Latitude in degrees * 1E7
 * @param alt Gumstix GPS Altitude in degrees * 1E7
 * @param eulerAngle Gumstix Euler Angle (roll, pitch, yaw) in degrees
 * @param angularRate Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 * @param controlSignal Gumstix Control Signals (aileron, elevator, pitch, yaw, throttle
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       int64_t time_usec, int32_t lon, int32_t lat, int32_t alt, const float *eulerAngle, const float *angularRate, const float *controlSignal)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, eulerAngle, 3);
	_mav_put_float_array(buf, 32, angularRate, 3);
	_mav_put_float_array(buf, 44, controlSignal, 5);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#else
	mavlink_gumstix_2_pxhawk_t packet;
	packet.time_usec = time_usec;
	packet.lon = lon;
	packet.lat = lat;
	packet.alt = alt;
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet.controlSignal, controlSignal, sizeof(float)*5);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_gumstix_2_pxHawk;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN, MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
}

/**
 * @brief Pack a gumstix_2_pxhawk message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_usec Gumstix onboard time in usec 
 * @param lon Gumstix GPS Longitude in degrees * 1E7
 * @param lat Gumstix GPS Latitude in degrees * 1E7
 * @param alt Gumstix GPS Altitude in degrees * 1E7
 * @param eulerAngle Gumstix Euler Angle (roll, pitch, yaw) in degrees
 * @param angularRate Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 * @param controlSignal Gumstix Control Signals (aileron, elevator, pitch, yaw, throttle
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           int64_t time_usec,int32_t lon,int32_t lat,int32_t alt,const float *eulerAngle,const float *angularRate,const float *controlSignal)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, eulerAngle, 3);
	_mav_put_float_array(buf, 32, angularRate, 3);
	_mav_put_float_array(buf, 44, controlSignal, 5);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#else
	mavlink_gumstix_2_pxhawk_t packet;
	packet.time_usec = time_usec;
	packet.lon = lon;
	packet.lat = lat;
	packet.alt = alt;
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet.controlSignal, controlSignal, sizeof(float)*5);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_gumstix_2_pxHawk;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN, MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
}

/**
 * @brief Encode a gumstix_2_pxhawk struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param gumstix_2_pxhawk C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_gumstix_2_pxhawk_t* gumstix_2_pxhawk)
{
	return mavlink_msg_gumstix_2_pxhawk_pack(system_id, component_id, msg, gumstix_2_pxhawk->time_usec, gumstix_2_pxhawk->lon, gumstix_2_pxhawk->lat, gumstix_2_pxhawk->alt, gumstix_2_pxhawk->eulerAngle, gumstix_2_pxhawk->angularRate, gumstix_2_pxhawk->controlSignal);
}

/**
 * @brief Encode a gumstix_2_pxhawk struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param gumstix_2_pxhawk C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_gumstix_2_pxhawk_t* gumstix_2_pxhawk)
{
	return mavlink_msg_gumstix_2_pxhawk_pack_chan(system_id, component_id, chan, msg, gumstix_2_pxhawk->time_usec, gumstix_2_pxhawk->lon, gumstix_2_pxhawk->lat, gumstix_2_pxhawk->alt, gumstix_2_pxhawk->eulerAngle, gumstix_2_pxhawk->angularRate, gumstix_2_pxhawk->controlSignal);
}

/**
 * @brief Send a gumstix_2_pxhawk message
 * @param chan MAVLink channel to send the message
 *
 * @param time_usec Gumstix onboard time in usec 
 * @param lon Gumstix GPS Longitude in degrees * 1E7
 * @param lat Gumstix GPS Latitude in degrees * 1E7
 * @param alt Gumstix GPS Altitude in degrees * 1E7
 * @param eulerAngle Gumstix Euler Angle (roll, pitch, yaw) in degrees
 * @param angularRate Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 * @param controlSignal Gumstix Control Signals (aileron, elevator, pitch, yaw, throttle
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_gumstix_2_pxhawk_send(mavlink_channel_t chan, int64_t time_usec, int32_t lon, int32_t lat, int32_t alt, const float *eulerAngle, const float *angularRate, const float *controlSignal)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, eulerAngle, 3);
	_mav_put_float_array(buf, 32, angularRate, 3);
	_mav_put_float_array(buf, 44, controlSignal, 5);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, buf, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN, MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, buf, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
#else
	mavlink_gumstix_2_pxhawk_t packet;
	packet.time_usec = time_usec;
	packet.lon = lon;
	packet.lat = lat;
	packet.alt = alt;
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet.controlSignal, controlSignal, sizeof(float)*5);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, (const char *)&packet, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN, MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, (const char *)&packet, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_gumstix_2_pxhawk_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  int64_t time_usec, int32_t lon, int32_t lat, int32_t alt, const float *eulerAngle, const float *angularRate, const float *controlSignal)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, eulerAngle, 3);
	_mav_put_float_array(buf, 32, angularRate, 3);
	_mav_put_float_array(buf, 44, controlSignal, 5);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, buf, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN, MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, buf, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
#else
	mavlink_gumstix_2_pxhawk_t *packet = (mavlink_gumstix_2_pxhawk_t *)msgbuf;
	packet->time_usec = time_usec;
	packet->lon = lon;
	packet->lat = lat;
	packet->alt = alt;
	mav_array_memcpy(packet->eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet->angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet->controlSignal, controlSignal, sizeof(float)*5);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, (const char *)packet, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN, MAVLINK_MSG_ID_gumstix_2_pxHawk_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_pxHawk, (const char *)packet, MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE gumstix_2_pxHawk UNPACKING


/**
 * @brief Get field time_usec from gumstix_2_pxhawk message
 *
 * @return Gumstix onboard time in usec 
 */
static inline int64_t mavlink_msg_gumstix_2_pxhawk_get_time_usec(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int64_t(msg,  0);
}

/**
 * @brief Get field lon from gumstix_2_pxhawk message
 *
 * @return Gumstix GPS Longitude in degrees * 1E7
 */
static inline int32_t mavlink_msg_gumstix_2_pxhawk_get_lon(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field lat from gumstix_2_pxhawk message
 *
 * @return Gumstix GPS Latitude in degrees * 1E7
 */
static inline int32_t mavlink_msg_gumstix_2_pxhawk_get_lat(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  12);
}

/**
 * @brief Get field alt from gumstix_2_pxhawk message
 *
 * @return Gumstix GPS Altitude in degrees * 1E7
 */
static inline int32_t mavlink_msg_gumstix_2_pxhawk_get_alt(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  16);
}

/**
 * @brief Get field eulerAngle from gumstix_2_pxhawk message
 *
 * @return Gumstix Euler Angle (roll, pitch, yaw) in degrees
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_get_eulerAngle(const mavlink_message_t* msg, float *eulerAngle)
{
	return _MAV_RETURN_float_array(msg, eulerAngle, 3,  20);
}

/**
 * @brief Get field angularRate from gumstix_2_pxhawk message
 *
 * @return Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_get_angularRate(const mavlink_message_t* msg, float *angularRate)
{
	return _MAV_RETURN_float_array(msg, angularRate, 3,  32);
}

/**
 * @brief Get field controlSignal from gumstix_2_pxhawk message
 *
 * @return Gumstix Control Signals (aileron, elevator, pitch, yaw, throttle
 */
static inline uint16_t mavlink_msg_gumstix_2_pxhawk_get_controlSignal(const mavlink_message_t* msg, float *controlSignal)
{
	return _MAV_RETURN_float_array(msg, controlSignal, 5,  44);
}

/**
 * @brief Decode a gumstix_2_pxhawk message into a struct
 *
 * @param msg The message to decode
 * @param gumstix_2_pxhawk C-struct to decode the message contents into
 */
static inline void mavlink_msg_gumstix_2_pxhawk_decode(const mavlink_message_t* msg, mavlink_gumstix_2_pxhawk_t* gumstix_2_pxhawk)
{
#if MAVLINK_NEED_BYTE_SWAP
	gumstix_2_pxhawk->time_usec = mavlink_msg_gumstix_2_pxhawk_get_time_usec(msg);
	gumstix_2_pxhawk->lon = mavlink_msg_gumstix_2_pxhawk_get_lon(msg);
	gumstix_2_pxhawk->lat = mavlink_msg_gumstix_2_pxhawk_get_lat(msg);
	gumstix_2_pxhawk->alt = mavlink_msg_gumstix_2_pxhawk_get_alt(msg);
	mavlink_msg_gumstix_2_pxhawk_get_eulerAngle(msg, gumstix_2_pxhawk->eulerAngle);
	mavlink_msg_gumstix_2_pxhawk_get_angularRate(msg, gumstix_2_pxhawk->angularRate);
	mavlink_msg_gumstix_2_pxhawk_get_controlSignal(msg, gumstix_2_pxhawk->controlSignal);
#else
	memcpy(gumstix_2_pxhawk, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_gumstix_2_pxHawk_LEN);
#endif
}
