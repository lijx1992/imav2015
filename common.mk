# This is an automatically generated record.
# The area between QNX Internal Start and QNX Internal End is controlled by
# the QNX IDE properties.

ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

#===== USEFILE - the file containing the usage message for the application. 
USEFILE=

# Next lines are for C++ projects only

EXTRA_SUFFIXES+=cxx cpp

#===== LDFLAGS - add the flags to the linker command line.
LDFLAGS+=-lang-c++ -Y _gpp

VFLAG_g=-gstabs+

#===== CCFLAGS - add the flags to the C compiler command line. 
CCFLAGS+=-D_GUMSTIX -Y _gpp

#===== LIBS - a space-separated list of library items to be included in the link.
LIBS+=m socket OpenCV-2.3.1 flexxes

#===== EXTRA_INCVPATH - a space-separated list of directories to search for include files.
EXTRA_INCVPATH+=$(QNX_TARGET)/usr/include/c++/4.4.2  \
	$(QNX_TARGET)/usr/include/c++/4.4.2/arm-unknown-nto-qnx6.5.0  \
	G:/LiJiaxin/common-libs/Reflexxes_src/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/calib3d/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/contrib/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/core/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/features2d/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/flann/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/highgui/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/imgproc/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/ml/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/objdetect/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/ts/include  \
	G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/modules/video/include  \
	G:/LiJiaxin/mavlink G:/LiJiaxin/aesEncryption

#===== EXTRA_LIBVPATH - a space-separated list of directories to search for library files.
EXTRA_LIBVPATH+=G:/LiJiaxin/common-libs/OpenCV-2.3.1-src/libForQNX  \
	G:/LiJiaxin/common-libs/Reflexxes_src/lib

#===== EXTRA_SRCVPATH - a space-separated list of directories to search for source files.
EXTRA_SRCVPATH+=G:/LiJiaxin/aesEncryption

include $(MKFILES_ROOT)/qmacros.mk
ifndef QNX_INTERNAL
QNX_INTERNAL=$(PROJECT_ROOT)/.qnx_internal.mk
endif
include $(QNX_INTERNAL)

include $(MKFILES_ROOT)/qtargets.mk

OPTIMIZE_TYPE_g=none
OPTIMIZE_TYPE=$(OPTIMIZE_TYPE_$(filter g, $(VARIANTS)))

