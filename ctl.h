#ifndef CTL_H_
#define CTL_H_

#include "matrix.h"
//#include "state.h"
#include "svo.h"
#include "uav.h"
#include "opencv2/opencv.hpp"
using namespace cv;


#include <ReflexxesAPI.h>
#include <RMLPositionFlags.h>
#include <RMLPositionInputParameters.h>
#include <RMLPositionOutputParameters.h>

#define CYCLE_TIME_IN_SECONDS		0.02
#define NUMBER_OF_DOFS				4
#define REFLEXXES_MAX_VEL_X			6.0
#define REFLEXXES_MAX_VEL_Y			6.0
#define REFLEXXES_MAX_VEL_Z			1.0
#define REFLEXXES_MAX_VEL_C			1.0		// yaw rate

#define REFLEXXES_MAX_ACC_X			1.5
#define REFLEXXES_MAX_ACC_Y			1.5
#define REFLEXXES_MAX_ACC_Z			0.5
#define REFLEXXES_MAX_ACC_C			0.5

#ifndef BOOL
#define BOOL	int
#define TRUE	1
#define FALSE	0
#endif

//CTL
#define NEW_OUTERLOOP
#define INNERLOOP_LQR				1
#define INNERLOOP_GAINSCHEDULE		2
#define INNERLOOP_DECOUPLE			3
#define INNERLOOP_CNF				4
#define INNERLOOP_RPT				5



#define BEHAVIOR_HFLY			11				//height keeping fly (u,v,r,h)
#define BEHAVIOR_TEST			13				//test
#define BEHAVIOR_FLY			15				//fly (u,v,w,r)
#define BEHAVIOR_HOLD			16				//hold (x,y,z,c)
#define BEHAVIOR_ENGINE			24				//engine, defaultly low
#define BEHAVIOR_ENGINEUP		26				//engine up
#define BEHAVIOR_ENGINEDOWN		27				//engine down
#define BEHAVIOR_TAKEOFF		28				//takeoff
#define BEHAVIOR_LAND			29				//land




#define BEHAVIOR_FLYTOGPS		38
#define BEHAVIOR_SEMIAUTO		37
#define BEHAVIOR_RETURNHOME		41
#define BEHAVIOR_WAYPOINTS		42

#define CONTROLFLAG_A1		0x00000001				//inner-loop Gain Scheduling, u = F*x
#define CONTROLFLAG_A2		0x00000002				//inner-loop RPT
#define CONTROLFLAG_A3		0x00000004				//inner-loop CNF, u = F*x + G*v + cnf part
#define CONTROLFLAG_A4		0x00000008				//servo driving test, only for behavior_h13
#define CONTROLFLAG_A5		0x00000010				//inner-loop decouple, u = F*x + G*v + cnf part (decoupled)
#define CONTROLFLAG_A6		0x00000020				//inner-loop LQR, u=F*x+G*v (originally A2)
#define CONTROLFLAG_A7		0x00000040				//reserved
#define CONTROLFLAG_A8		0x00000080				//engine up and down
#define CONTROLFLAG_A9		0x00000100				//direct servo position
#define CONTROLFLAG_B1		0x00001000				//reserved
#define CONTROLFLAG_B2		0x00002000				//outer-loop (original B5)
#define CONTROLFLAG_B3		0x00004000				//reserved
#define CONTROLFLAG_B4		0x00008000				//height keeping
#define CONTROLFLAG_B5		0x00010000				//outer-loop trajctory tracking (@)
#define CONTROLFLAG_B6		0x00020000				//chirp signal
#define CONTROLFLAG_OUTERLOOP_QUADLION		0x00040000				// outerloop for QuadLion
#define CONTROLFLAG_B8		0x00080000				//reserved
#define CONTROLFLAG_B9		0x00100000				//head to
#define CONTROLFLAG_B10		0x00200000				//reserved
#define CONTROLFLAG_B11		0x00400000				//outer-loop for aiming control
#define CONTROLFLAG_B12		0x00800000				//outer-loop for velocity feedback control
#define CONTROLFLAG_B13		0x01000000				//outer-loop for heading yawing control
#define CONTROLFLAG_B14		0x02000000				//outer-loop for virtual leader-follower
#define CONTROLFLAG_INNERLOOP_QUADLION		0x04000000		//PI outerloop control, almost same as B5
#define CONTROLFLAG_OUTERLOOP_SEMI		0x08000000		//
#define CONTROLFLAG_INNERLOOP_SEMI		0x10000000
#define CONTROLFLAG_MANUAL_INNERLOOP			0x20000000		/*!< Used to track manual input */

#define CHIRPADD_OUTERLOOP		1
#define CHIRPADD_CONTROLSIGNAL	2

#define MAX_CTL	128

//#define THROTTLE_LOW		0.8				//engine low, clutch on
#define THROTTLE_LOW		0.92				//engine low, clutch on
#define THROTTLE_ENTRY		0.67			//governer entry
#define THROTTLE_ENTER		0.6				//a point in the governer entry
#define THROTTLE_HIGH		0.5				//governer managed, used for in-air control

#define THROTTLE_SHUTDOWN	-0.75
#define LAND_HEIGHT		0.25		// 0.25m to land
#define MAX_THROTTLE	0.85

#define AUXILIARY_LOW		0.1				//no lifting
#define AUXILIARY_HOVER		-0.227

#define A8MODE_ENGINEUP			1
#define A8MODE_ENGINEDOWN		2
#define A8MODE_AUXILIARYDOWN	3

#define A9FLAG_AILERON		0x0001
#define A9FLAG_ELEVATOR		0x0002
#define A9FLAG_AUXILIARY	0x0004
#define A9FLAG_RUDDER		0x0008
#define A9FLAG_THROTTLE		0x0010
#define A9FLAG_ALL			0xffff

#define MODE_ALLAUTO	1	// all manual channels to semi-auto
#define MODE_SEMIAUTO	2	// all manual channels except throttle to semi-auto
#define MODE_NAVIGATION	3	// purely auto control and navigation

#define MANUAL_DEADZON	0.03
#define MAX_LONSPEED		20
#define MAX_LATSPEED		20
#define MAX_VERSPEED		10
#define MAX_YAWRATE			PI
#define HEADINGERR_DEADZONE 5*PI/180
#define GET_TRIMVALUE	1		// test(1) command
#define RESET_TRIMVALUE 2		// test(2) command
#define WP1_TRACK		3		// waypoint1 tracking
#define WP2_TRACK		4		// waypoint2 tracking
#define WP3_TRACK		5		// waypoint3 tracking
#define UAVFORGE_DST	6
#define INTEGRATOR_START 11
#define INTEGRATOR_STOP	 12
#define POSITION_ADJUST  13
#define TEST_MODE_SEMIAUTO	14
#define TEST_MODE_RESET		15
#define MODE_NOGPS			16
#define SMOOTH_SWEEP		17
#define NO_SMOOTH_SWEEP		18
#define TAKEOFF_ADJUST		19
#define SIMULATION_IMUSTUCK	20
#define SYSTEM_REBOOT		21

#define MAX_ACCB	3

#define XERRINT_MIN		-12
#define XERRINT_MAX		12
#define YERRINT_MIN		-12
#define YERRINT_MAX		12
#define ZERRINT_MIN		-3
#define ZERRINT_MAX		3
#define CERRINT_MIN		-0.1745		// -10 deg
#define CERRINT_MAX		0.1745		// 10 deg

#define TRACKING_MAX_VEL	2
#define TRACKING_MAX_ACC	0.4

#define MAXSIZE_BEHAVIORPARAMETER	128


static const int STATUS_GROUND = 0;
static const int STATUS_AIR = 1;
static const float Z_IN_AIR = -0.5;	/*!< 0.5 meter height above the ground considers in the air */

#ifdef PIXHAWK
static const double THROTTLE_SIG_INAIR = 0.3;
#else
static const double THROTTLE_SIG_INAIR = -0.2;
#endif

struct BEHAVIOR {
	int behavior;
	char parameter[MAXSIZE_BEHAVIORPARAMETER];
};

// NED GPS location
struct LOCATION {
	double latitude;
	double longitude;
	double altitude;
};

// Heading frame location
struct HLOCATION {
	double x;
	double y;
	double z;
};

/*!
 * \brief Outer-loop reference
 */
struct QROTOR_REF    // current input to the inner loop
{
	double p_x_r;	/*!< NED position reference in x axis, in m */
	double p_y_r;	/*!< NED position reference in y axis, in m */
	double p_z_r;	/*!< NED position reference in z axis, in m */
	double v_x_r;	/*!< NED velocity reference in x axis, in m/s */
	double v_y_r;	/*!< NED velocity reference in y axis, in m/s */
	double v_z_r;	/*!< NED velocity reference in z axis, in m/s */
	double agx_r;	/*!< NED acceleration reference in x axis, in m/s^2 */
	double agy_r;
	double agz_r;
	double psi_r;	/*!< NED heading reference, rad */
	double r_r;		/*!< NED heading rate reference, rad/s */
};


#pragma pack(push, 4)
struct CONTROLSTATE {
	int nBehavior;
	int fControl;

	double u,v,w,r;				//outerloop setting
	double vChirp[4];

	double tmp[16];
//	char reserve[128-8*sizeof(double)-2*sizeof(int)];
};				//control setting for inner-loop control
#pragma pack(pop)



#define MAX_PLAN	10				//maximum ten plans

class clsPath;

class clsPlan {
public:
	clsPlan();
	virtual ~clsPlan();

protected:
	UAVSTATE m_state;

public:
	void SetState(UAVSTATE *pState) { m_state = *pState; }

protected:
	int m_planID;
	BEHAVIOR m_behavior;
	double m_tStart;

public:
	int GetPlanID() const { return m_planID; }
	void SetPlanID(int nPlan) { m_planID = nPlan; }
	void GetBehavior(BEHAVIOR *pBehavior) { *pBehavior = m_behavior; }

public:
	virtual void Reset() {};
	virtual int Run() = 0;
};



// Li Jiaxin
class clsFullTaskPlan : public clsPlan {
protected:
	enum { READY, ENGINEUP, ASCEND, HOLD, NAVIGATE, LAND} m_mode;
	double m_tHoldStart;
public:
	void Reset();
	int Run();
};


struct UVWR {
	double u, v, w, r;				//the reference signal for inner-loop control
};

struct SMOOTHPATH {
	double vCruise;
	double acc;
	int nPoints;
	int curPoint;
//	int ntest;
	char waypoints[512];
};

struct WAYPOINTS {
	int nPoints;		/*!< total number of way points uploaded from GCS */
	int curPoint;		/*!< current point which is tracking */
	char waypoints[9600];	/*!< series of points with GPS lat, long, alt */
};


struct FFcontrol
{
	double Accgxc; /* command of ground acceleration along north direction */
	double Accgyc; /* command of ground acceleration along East direction */
	double dpsic; /* command of yaw rate */
	double Wc; /* command of heaving speed */
};

class clsCTL : public clsThread {
public:
	clsCTL();
	virtual ~clsCTL();

	void Init();

public:
	virtual BOOL InitThread();
	virtual int EveryRun();
	virtual void ExitThread();

protected:
	COMMAND m_cmd;
	pthread_mutex_t m_mtxCmd;

public:
	void PutCommand(COMMAND *pCmd);
	void GetCommand(COMMAND *pCmd);

	BOOL ProcessCommand(COMMAND *pCommand);

protected:
	clsPlan *m_pPlan;

	clsPath* m_pFlPath, *m_pLdPath;
	clsFullTaskPlan m_planFullTask;

//Li Jiaxin
private:
	BOOL m_bTakeOffFlag;
	bool flag_FULL_PLAN_SET;

public:
	void SetPlan(int nPlan);
	//Li Jiaxin
	BOOL GetTakeOffFlag() { return m_bTakeOffFlag;}
	void SetTakeOffFlag() { m_bTakeOffFlag = TRUE; }
	void ResetTakeOffFlag() { m_bTakeOffFlag = FALSE; }

	void SetFlag_FULL_PLAN(){flag_FULL_PLAN_SET = true;}
	void ResetFlag_FULL_PLAN(){flag_FULL_PLAN_SET = false;}
	bool GetFlag_FULL_PLAN(){return flag_FULL_PLAN_SET;}

protected:
	double m_tNotify;				//this is used to kept the record of notify command, which is used for datalink check
	BOOL m_bNotify;

	BOOL m_bGPSDeny;
	BOOL m_bUseRefInOuter;
protected:
	//m_behavior to receive the behavior command, m_behaviorCurrent to indicate the behavior currently being excuted
	BEHAVIOR m_behavior;

	BEHAVIOR m_behaviorCurrent;

	int m_fControl;

	HELICOPTERRUDDER m_sig, m_semiSig, m_manualSig;				//output control signal
	double m_throttleCur;
	double m_camera;				//camera pitch angle
	BOOL m_bCamTrack;
	double m_dRudderFactor;

	double m_curX, m_curY, m_curZ;

protected:
	double m_innerloop;				//decide which block is used to implement the inner-loop control
									//so far options include lqr, cnf and decouple
	int m_nPath;	// the path# currently is tracking

	double m_cc; 		// ground frame heading reference for inner loop

	EQUILIBRIUM		m_equ;				//equilibrium for all controls

public:
	EQUILIBRIUM A2_equ;

protected:
	//A4 parameter
	EQUILIBRIUM A4_equ;

	double A4_t0;
	double A4_nTest;

	//A5 parameter
	EQUILIBRIUM A5_equ;

	double _A5_F1[2][8]; clsMatrix A5_F1;
	double _A5_G1[2][2]; clsMatrix A5_G1;
	double _A5_K1[2][2]; clsMatrix A5_K1;
	double _A5_L1[2][8]; clsMatrix A5_L1;
	double _A5_M1[2][2]; clsMatrix A5_M1;

	double A5_al1, A5_al2;
	double A5_be1, A5_be2;

	double _A5_P1[2][2]; clsMatrix A5_P1;
	double _A5_Q1[2][2]; clsMatrix A5_Q1;

	double _A5_F4[2][4]; clsMatrix A5_F4;
	double _A5_G4[2][2]; clsMatrix A5_G4;
	double _A5_K4[2][4]; clsMatrix A5_K4;
	double _A5_M4[4][2]; clsMatrix A5_M4;

	double A5_al4;
	double A5_be4;

	//inner-loop control parameter
	double A1A2A3_u;
	double A1A2A3_v;
	double A1A2A3_w;
	double A1A2A3_r;

	double A5_u, A5_v, A5_w, A5_c;

	//A6 parameter
	double _A6_F[4][11];
	clsMatrix A6_F;

	double _A6_G[4][4];
	clsMatrix A6_G;

	//A7 parameter
	EQUILIBRIUM A7_equ;

	double _A7_F[4][11];	clsMatrix A7_F;
	double _A7_G[4][4];		clsMatrix A7_G;

	double _A7_Fr[4];		clsVector A7_Fr;
	double A7_Gr;

	double _A7_Nr[4];		clsVector A7_Nr;
	double _A7_Ger[4];		clsVector A7_Ger;

	double A7_al, A7_be;

	double A7_t0;
	double A7_bOpen;

	double A7_r;
	double A7_er;				//open loop setting

	//A8 parameter
	int A8_mode;

	double A8_t0;
	HELICOPTERRUDDER A8_sig0;

	BOOL A8_bEnd;

	//A9 parameter
	int A9_flag;
	double A9_ea, A9_ee, A9_eu, A9_er, A9_et;

	//outerloop control parameter B1-B9

	//b2
	double B2_t0;
	double B2_x, B2_y, B2_z, B2_c;				//for hover

	double B2_t;

	clsPath *B2_pPath;				//path to track
	int B2_mode;				//path tracking mode
	
	double B2_x0, B2_y0, B2_z0, B2_c0;				//initial position for path tracking
	double B2_kx, B2_ky, B2_kz, B2_kc;				//feedback gains

	double B2_dxi, B2_dyi, B2_dzi, B2_dci;
	double B2_kxi, B2_kyi, B2_kzi, B2_kci;

	BOOL B2_bEnd;

	//b2
	double B4_z;
	double B4_kz;

	//b5 variables
	double B5_t0;
	double B5_semiPsi, B5_semiPsic, B5_PsiErr;
	double B5_t1, B5_t2, B5_t3, B5_t4;

	double B5_x, B5_y, B5_z, B5_c;					//for hover
	double B5_xref, B5_yref, B5_zref, B5_cref;

	clsPath *B5_pPath, *B5_pPath2;				//B5_pPath2 for additionally addon path, may used in formation
	double B5_tPath;		// the time passed during the path tracking
	int B5_outerloopMode;

	double B5_x0, B5_y0, B5_z0, B5_c0;				//for path tracking, initial position and heading angle
	double B5_kx, B5_ky, B5_kz, B5_kc;				//control parameters
	double B5_adjustc;					// adjustment in ground frame heading tracking purpose

	double B5_t;
	double B5_dxi, B5_dyi, B5_dzi, B5_dci;				//control parameters
	double B5_kxi, B5_kyi, B5_kzi, B5_kci;				//control parameters

	BOOL B5_bEnd;
	int B5_mode;

	double _B5_F[3][6];
	clsMatrix B5_F;
	double _B5_G[3][9];
	clsMatrix B5_G;

	double _B5_CMDGEN_A[2][2]; clsMatrix B5_CMDGEN_A;
	double _B5_CMDGEN_B[2];	clsVector B5_CMDGEN_B;
	double B5_vax[2], B5_vay[2], B5_vaz[2];
	double B5_vac;

	double _B5_F_GREMLION_RPT[3][6]; clsMatrix B5_F_GREMLION_RPT;
	double _B5_G_GREMLION_RPT[3][9]; clsMatrix B5_G_GREMLION_RPT;

	double _B5_Aorg_GREMLION[2][2]; clsMatrix B5_Aorg_GREMLION;
	double _B5_Borg_GREMLION[2]; clsVector B5_Borg_GREMLION;

	double _B5_Aorg2_GREMLION[2][2]; clsMatrix B5_Aorg2_GREMLION;
	double _B5_Aorg3_GREMLION[2][2]; clsMatrix B5_Aorg3_GREMLION;

	double _B5_F_GREMLION[3][6];
	clsMatrix B5_F_GREMLION;
	double _B5_G_GREMLION[3][3];
	clsMatrix B5_G_GREMLION;
	double B5_FVn;

public:
	double B5_pnr[4], B5_vnr[4], B5_anr[3];	// p, v, a ref in NED for GremLion
protected:
	BOOL B5_bSemi1stFlag;
	BOOL B6_bChirp;				//if chirp signal is on
	double B6_t0;
	double B6_vChirp[4];
	int B6_add;

	double _OUTER_P_QUADLION[4][7]; clsMatrix OUTER_P_QUADLION;
	double _OUTER_D_QUADLION[4][7]; clsMatrix OUTER_D_QUADLION;
	double _Fxy[5]; clsVector Fxy;

	double _FzSim[3]; clsVector FzSim;
	double _Fz[5]; clsVector Fz;

	double _Fc[3]; clsVector Fc;
	double dc_ail2phi, dc_ele2tht, dc_thr2w, dc_rud2r;
	double damping_u, damping_v, ratio_u, ratio_v;
	double m_xerrint, m_yerrint, m_zerrint, m_cerrint;
	BOOL m_bIntegrator;

	int B6_channel;
	double B6_a;
	double B6_om1;
	double B6_om2;
	double B6_T;

	double B7_x, B7_y, B7_z, B7_c;
	double B7_kx;
	double B7_ky;
	double B7_kz;
	double B7_kc;

	double B8_c;
	double B8_kc;

	double B9_x, B9_y, B9_z;
	double B9_kx;
	double B9_ky;
	double B9_kz;
	double B9_kc;
	double B9_zc;
	double B9_lockNED[3];	// camtrack locked position in NED
	double B9_xLock, B9_yLock, B9_zLock;	// camtrack locked position in body frame

	double B10_t;

	double B10_x, B10_y, B10_z;
	double B10_kx, B10_ky, B10_kz;

	double B10_dxi, B10_dyi, B10_dzi;
	double B10_kxi, B10_kyi, B10_kzi;

	clsPath *B11_pPath;
	int B11_mode;

	double B11_t0;
	double B11_x0, B11_y0, B11_z0;
	double B11_c0;

	double B11_xt, B11_yt, B11_zt;

	double B11_kx, B11_ky, B11_kz, B11_kc;

	double B12_u, B12_v, B12_w, B12_r;
	double B12_ku, B12_kv, B12_kw, B12_kr;

	double B13_t0;

	double B13_x, B13_y, B13_z, B13_c;

	clsPath *B13_pPath;
	int B13_mode;

	double B13_x0, B13_y0, B13_z0, B13_c0;

	double B13_kx, B13_ky, B13_kz;

	double B13_t;
	double B13_dxi, B13_dyi, B13_dzi;				//control parameters
	double B13_kxi, B13_kyi, B13_kzi;				//control parameters

	BOOL B13_bEnd;				//to judge if the path is ended

//B14 virtual leader-follower
	double B14_t0;

	clsPath *B14_pPath;

	double B14_x0, B14_y0, B14_z0, B14_c0;				//for path tracking, initial position and heading angle
	double B14_kx, B14_ky, B14_kz, B14_kc;				//control parameters

	double B14_t;
	double B14_dxi, B14_dyi, B14_dzi, B14_dci;				//control parameters
	double B14_kxi, B14_kyi, B14_kzi, B14_kci;				//control parameters

// B15: B5 added with PI control
	double B15_t0;

	double B15_x, B15_y, B15_z, B15_c;					//for hover

	clsPath *B15_pPath, *B15_pPath2;				//B5_pPath2 for additionally addon path, may used in formation
	int B15_mode;

	//Li Jiaxin
	FILE *FileIndoorMultiWaypoint, *FileOutdoorMultiWaypoint;
	double WaypointIndoorMultiWaypoint[30][4];
	int n_indoorWaypoints,WaypointVisiting;
	bool outdoor_guide_init;
	double GPSCoordinate[30][4];
	double WaypointOutdoorMultiWaypoint[30][4];
	int n_outdoorWaypoints;
	double B15_x0, B15_y0, B15_z0, B15_c0;				//for path tracking, initial position and heading angle
	double B15_kx, B15_ky, B15_kz, B15_kc;				//control parameters

	double B15_t;
	double B15_dxi, B15_dyi, B15_dzi, B15_dci;				//control parameters
	double B15_kxi, B15_kyi, B15_kzi, B15_kci;				//control parameters
	BOOL B15_bEnd;

	double B_acxr_ub, B_acyr_vb, B_aczr_wb, B_cr;
	double _B_psi[3]; 
	double _pva[3][3]; 
	double _F_F[3][3]; clsMatrix F_F;
	double _F_H[1][3]; clsMatrix F_H;
	double _F_H2[2][3]; clsMatrix F_H2;
	double _F_P[3][3]; clsMatrix F_P;
	double _F_P2[3][3]; clsMatrix F_P2;
	double _F_R[1][1]; clsMatrix F_R;
	double _F_R2[2][2]; clsMatrix F_R2;
	double _F_Q[3][3]; clsMatrix F_Q;
	double _F_K[3][1]; clsMatrix F_K;
	double _F_K2[3][2]; clsMatrix F_K2;
	BOOL m_bFK;		// flag for formation kalman
	double m_ldHeading;
	int m_nHeading;
	
	int m_nLandCnt;
	int m_nThrottleMode;
	int m_nThrottleCase;
	BOOL m_bThrottleBypass, m_bThrMode3, m_bThrMode1;
	BOOL m_bLandCmd, m_bLandFlag;
	BOOL m_bSyncPath;	/*!< Flag to indicate sync path */
	BOOL m_bSyncStart;
	unsigned int m_utcMin;

	double _A1_A[11][11]; clsMatrix A1_A;
	double _A1_B[11][6];  clsMatrix A1_B;
	double _A1_C[3][11];  clsMatrix A1_C;
	double _A1_G[3][3];   clsMatrix A1_G;
	double _A1_K[11][3];  clsMatrix A1_K;
	double _A1_x[11];
	double _A1_x_raw[11];

	double A1_ref_a;
	double A1_ref_b;
	double A1_ref_c;
	double A1_throttle;
	EQUILIBRIUM A1_equ;

	//B3 parameter
	double _B3_A[6][6]; clsMatrix B3_A;
	double _B3_B[6][3]; clsMatrix B3_B;
	double _B3_C[3][6]; clsMatrix B3_C;
	double _B3_G[3][9]; clsMatrix B3_G;
	double _B3_K[6][9]; clsMatrix B3_K;
	double _B3_x[6];

	double B2_xref, B2_yref, B2_zref, B2_cref;

	double B2_z_P, B2_z_I, B2_z_D;
	double B2_kx_P, B2_kx_D, B2_kx_I, B2_x_I, B2_c_I;
	double B2_ky_P, B2_ky_D, B2_ky_I, B2_y_I;
	double B2_kz_P, B2_kz_I, B2_kz_D;
	double B2_kc_P, B2_kc_I, B2_kc_D;

	double ref_x, ref_y, ref_z;
	double ref_u, ref_v, ref_w;
	double ref_acx, ref_acy, ref_acz;

	BOOL m_bAutoPath;

	SMOOTHPATH m_smoothPath;
	WAYPOINTS m_wayPoints;
	BOOL m_bPathSmooth, m_bIndoorPath, num1, num2;
	int m_waypointIndex;


	double m_psir;
	// output variables of Formation Control Algorithm
	FFcontrol m_UAVcontrol;
	//Li Jiaxin
	Mat m_DCgain;

	BOOL m_bvisionFormation, m_bvisionFormationCloseloop;
	double m_visionKFstate[9];
	double m_ctlPan, m_ctlTilt;
	double m_xTrapezoid[4], m_yTrapezoid[4], m_zTrapezoid[4];
	double m_tTrackingFinal;

	BOOL m_bAdjust;
	BOOL m_bNoGPS;
	BOOL m_bInAir;

public:
	double m_abcRef[3];

protected:
	void A8();				//for yaw control

	void Outerloop_QuadLion();
	void Innerloop_QuadLion();
	void Outerloop_Semi();
	void Innerloop_Semi();
	void Innerloop_Manual();

	void SetBehavior(BEHAVIOR *pBeh);				//uniform setbehavior

	void SetBehavior(int nBehavior);				//nBehavior == BEHAVIOR_H1(hover)
	void SetBehavior(int nBehavior, int nTest);		//BEHAVIOR_H13, test
	void SetBehavior(int nBehavior, double d1);				//BEHAVIOR_H2(lift), H3(disecnd)
	void SetBehavior(int nBehavior, double d1, double d2, double d3, double d4);				//BEHAVIOR_H5, H10, H11, H15, H16

	void SetBehavior(int nBehavior, double pos1[4], double pos2[4]);				//BEHAVIOR_TAKEOFFX and BEHAVIOR_LANDX

	void AutoPathGeneration();
	void GCSRefGeneration();

protected:
    ReflexxesAPI				*RML;	/*!< Reflexxes RML API variable */
    RMLPositionInputParameters	*IP;	/*!< Reflexxes input variable */
    RMLPositionOutputParameters	*OP;	/*!< Reflexxes output variable */
    RMLPositionFlags			Flags;	/*!< Reflexxes position flags */

    ReflexxesAPI				*m_velRML;		/*!< Reflexxes velocity API */
    RMLVelocityInputParameters	*m_velIP;	/*!< Reflexxes velocity mode input */
    RMLVelocityOutputParameters	*m_velOP;	/*!< Reflexxes velocity mode output */
    RMLVelocityFlags			m_velFlags;	/*!< Reflexxes velocity mode flags */

    QROTOR_REF m_reflexRef;
    //Li Jiaxin
    BOOL m_bReflexxesCall, m_bTakeoff, m_bGPSPath, m_bReflexxesInit, m_bReturnHome, m_bHFLY, m_bHold, m_bLand, m_bSemiAuto, m_bSemiAutoInit, m_bWaypoints, m_bIndoorNav,m_bIndoorMultiWaypoint,m_bOutdoorMultiWaypoint,
    		m_bImageLog, m_bSweepPattern, m_bSmoothSweep;
    BOOL m_bTakeoffAdjust, m_bDisarmFlag;
    double m_reflexxesSyncTime, m_tTakeoff, m_tGPSPath, m_tReturnHome, m_tHFLY, m_tHold, m_tLand;
    double m_tHoverWP;
    double m_takeoffPos[2], m_planPara;
    double m_flyToGPSLong, m_flyToGPSLat;
    double m_tSweepStartTime, m_sweepVel, m_sweepDist;

    double m_semipnr[4];

    LOCATION m_aSweepLOC[4]; /*!< 4 sweep GPS locations */
    HLOCATION m_aSweepNED[4];	/*!< 4 sweep locations in local NED */

    // Li Jiaxin
    int FlyToGpsIndex;

public:
    double m_takeOffsetX, m_takeOffsetY, m_cLeader;

public:
	BOOL isGPSDenied() { return m_bGPSDeny; }
	void SetGPSDeny() { m_bGPSDeny = TRUE; }
	void ResetGPSDeny() { m_bGPSDeny = FALSE; }

	BOOL isRefUseInOuter() { return m_bUseRefInOuter; }
	void SetRefUseInOuter() { m_bUseRefInOuter = TRUE; }
	void ResetRefUseInOuter() { m_bUseRefInOuter = FALSE; }

public:
	void SetObserver0();
	void SetObserver1();
	void SetObserver2();

public:
	void FormationKalman(double pos[4], double vel[3], double acc[3], double psider[2]);
public:
	clsPath *GetPath(int nPath);

public:
	int GetMode() const { return B5_outerloopMode; }
	void SetMode(int mode) { B5_outerloopMode = mode; }
	void SetSemiPsi(double psi) { B5_semiPsi = psi; }
	double GetSemiPsi()	{ return B5_semiPsi; }
	BOOL GetThrottleByPassFlag() { return m_bThrottleBypass; }

	void SetPathSmooth() { m_bPathSmooth = TRUE; }
	BOOL IsPathSmooth() { return m_bPathSmooth? TRUE: FALSE; }
	void SetIndoorPath() { m_bIndoorPath = TRUE; }
	void ResetIndoorPath() { m_bIndoorPath = FALSE; }
	BOOL IsIndoorPath() { return m_bIndoorPath? TRUE: FALSE; }
	void ResetPathSmooth() { m_bPathSmooth = FALSE; }
	double GetPathStartTime() { return B5_t0; }

	void SetIntegratorFlag() { m_bIntegrator = TRUE; }
	void ResetIntegratorFlag() { m_bIntegrator = FALSE; }
	BOOL GetIntegratorFlag() const { return m_bIntegrator; }


	void SetAdjustFlag() { m_bAdjust = TRUE; }
	void ResetAdjustFlag() { m_bAdjust = FALSE; }
	BOOL GetAdjustFlag() const { return m_bAdjust; }

	void SetNoGPSFlag() { m_bNoGPS = TRUE; }
	void ResetNoGPSFlag() { m_bNoGPS = FALSE; }
	BOOL GetNoGPSFlag() const { return m_bNoGPS; }

	void SetReflexxesCallFlag() { m_bReflexxesCall = TRUE; }
	void ResetReflexxesCallFlag() { m_bReflexxesCall = FALSE; }
	BOOL GetReflexxesCallFlag() const { return m_bReflexxesCall; }

	void SetReflexxesInitFlag() { m_bReflexxesInit = TRUE; }
	void ResetReflexxesInitFlag() { m_bReflexxesInit = FALSE; }
	BOOL GetReflexxesInitFlag() const { return m_bReflexxesInit; }

	void SetTakeoffFlag() { m_bTakeoff = TRUE; }
	void ResetTakeoffFlag() { m_bTakeoff = FALSE; }
	BOOL GetTakeoffFlag() const { return m_bTakeoff; }

	void SetLandFlag()	{ m_bLand = TRUE; }
	void ResetLandFlag() { m_bLand = FALSE; }
	BOOL GetLandFlag()	const { return m_bLand; }


	void SetReturnHomeFlag() { m_bReturnHome = TRUE; }
	void ResetReturnHomeFlag() { m_bReturnHome = FALSE; }
	BOOL GetReturnHomeFlag() const { return m_bReturnHome; }

	void SetHFLYFlag()	{ m_bHFLY = TRUE; }
	void ResetHFLYFlag() { m_bHFLY = FALSE; }
	BOOL GetHFLYFlag() const { return m_bHFLY; }

	void SetHoldFlag() { m_bHold = TRUE; }
	void ResetHoldFlag() { m_bHold = FALSE; }
	BOOL GetHoldFlag() const { return m_bHold; }

	void SetWaypointsFlag() { m_bWaypoints = TRUE; }
	void ResetWaypointsFlag() { m_bWaypoints = FALSE; }
	BOOL GetWaypointsFlag() const { return m_bWaypoints; }

	void SetSmoothSweepFlag() { m_bSmoothSweep = TRUE; }
	void ResetSmoothSweepFlag() { m_bSmoothSweep = FALSE; }
	BOOL GetSmoothSweepFlag() const { return m_bSmoothSweep; }

	void SetTakeoffAdjustFlag() { m_bTakeoffAdjust = TRUE; }
	void ResetTakeoffAdjustFlag() { m_bTakeoffAdjust = FALSE; }
	BOOL GetTakeoffAdjustFlag() const { return m_bTakeoffAdjust; }

	void SetImageLogFlag() { m_bImageLog = TRUE; }
	void ResetImageLogFlag() { m_bImageLog = FALSE; }
	BOOL GetImageLogFlag() const { return m_bImageLog; }

	void SetSemiAutoFlag() { m_bSemiAuto = TRUE; }
	void ResetSemiAutoFlag() { m_bSemiAuto = FALSE; }
	BOOL GetSemiAutoFlag() const { return m_bSemiAuto; }

	void SetSemiAutoInitFlag() { m_bSemiAutoInit = TRUE; }
	void ResetSemiAutoInitFlag() { m_bSemiAutoInit = FALSE; }
	BOOL GetSemiAutoInitFlag() const { return m_bSemiAutoInit; }

	void SetIndoorNavFlag() { m_bIndoorNav = TRUE; }
	void ResetIndoorNavFlag() { m_bIndoorNav = FALSE; }
	BOOL GetIndoorNavFlag() const { return m_bIndoorNav; }

	void SetIndoorMultiWaypointFlag() {m_bIndoorMultiWaypoint = TRUE;}
	void ResetIndoorMultiWaypointFlag() {m_bIndoorMultiWaypoint = FALSE;}
	BOOL GetIndoorMultiWaypointFlag() {return m_bIndoorMultiWaypoint;}

	void SetOutdoorMultiWaypointFlag() {m_bOutdoorMultiWaypoint = TRUE;}
	void ResetOutdoorMultiWaypointFlag() {m_bOutdoorMultiWaypoint = FALSE;}
	BOOL GetOutdoorMultiWaypointFlag() {return m_bOutdoorMultiWaypoint;}


	void SetTakeoffPos(double x, double y);
	void GetTakeoffPos(double &x, double &y);

	int GetSyncPathFlag() const { return m_bSyncStart; }

	void SetTakeoffHeight(double height) { m_planPara = height; }
	double GetTakeoffHeight() const { return m_planPara; }


	void ProcessCtlMode();
	int GetUAVStatus();
	void SetInAIRFlag() { m_bInAir = TRUE; }
	void ResetInAirFlag() { m_bInAir = FALSE; }
	BOOL GetInAirFlag() const { return m_bInAir; }


	void ReflexxesPathPlanning(UAVSTATE state, QROTOR_REF ref, double pnr[4], double vnr[3], double anr[3]);
	void ReflexxesPathPlanning(UAVSTATE state, QROTOR_REF ref, double &t, double pnr[4], double vnr[3], double anr[3]);
	void ReflexxesPathPlanning(QROTOR_REF ref, double pnr[4], double vnr[3], double anr[3]);
	void ReflexxesPathPlanningChangeParas(QROTOR_REF ref, double pnr[4], double vnr[3], double anr[3]);
	void ReflexxesPathPlanningVelocityMode(UAVSTATE state, QROTOR_REF ref, double pnr[4], double vnr[4], double anr[3]);

	void GetTakeoffRef();
	void GetGPSPathRef();
	void GetReturnHomeRef();
	void GetHFLYRef();
	void GetSemiAutoRef();
	void GetSemiAutoRefInVelocityMode();
	void GetHoldRef();
	void GetLandRef();
	void GetWaypointsRef(WAYPOINTS &waypoints);
	void GetCurrentWaypoint(char *pWaypoints, int nCurPoint, double curPos[4]);


	void SetDisarmFlag() { m_bDisarmFlag = TRUE; }
	void ResetDisaarmFlag() { m_bDisarmFlag = FALSE; }
	BOOL GetDisarmFlag() const { return m_bDisarmFlag; }

	BOOL GetTrackingOKFlag();


protected:
	int m_nCTL;
	double m_tCTL[MAX_CTL];
	CONTROLSTATE m_ctl[MAX_CTL];				//inner loop signal - {u,v,w,p,q,r,ug,vg,wg}

	pthread_mutex_t m_mtxCTL;

	friend class clsDLG;
	friend class clsCoop;
	friend class clsTmpPath;
	friend class clsTakeoffPlan;
	friend class clsLandPlan;
};

//path
#define PATHTYPE_DYNAMIC		1
#define PATHTYPE_STATIC			2
#define PATHTYPE_DYNAMICX		3

#define PATHTRACKING_FIXED		0x0001
#define PATHTRACKING_ADDON		0x0002
#define PATHTRACKING_REPEAT		0x0004				//repeat while path is end

#define PATHTYPE_CIRCLE		7
#define PATHTYPE_CIRCLE8	8



#define MAX_TMPPATHSIZE		10000


#endif				//CTL_H_
