
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
//#include <dos.h>
#include <vector>
#include <math.h>

#include "opencv2/opencv.hpp"
//#include "formationLiao.h"

using namespace cv;
using namespace std;

void cmdconvertor(Mat ya, Mat ref, Mat &cmd){

	double m_Vgbh[2][1]       = {ya.at<double>(0,0), ya.at<double>(1,0)};
	double phi				  = ya.at<double>(6,0);
	double theta			  = ya.at<double>(7,0);
	double psi			   	  = ya.at<double>(8,0);
	double m_anb[3][1]		  = {ya.at<double>(15,0), ya.at<double>(16,0), ya.at<double>(17,0)};
	double m_agghref[2][1]    = {ref.at<double>(0,0), ref.at<double>(1,0)};
	double Vggzref		      = ref.at<double>(2,0);
	double dpsic		      = ref.at<double>(3,0);
	double m_Bpsi[2][2]       = {{cos(psi), sin(psi)}, {-sin(psi),cos(psi)}};
	double m_E23[2][3]   	  = {{0, 1, 0}, {0, 0, 1}};
	double sinphia, phia, phic, sinthetaa, thetaa, thetac, Vgbzc;
	double m_a[1][3]          = {1, 0, 0};
	double temp1[1][2]         = {-sin(theta), sin(phi)*cos(theta)};


	Mat Vgbh	= Mat(2, 1, CV_64F, m_Vgbh);
	Mat anb     = Mat(3, 1, CV_64F, m_anb );
	Mat agghref = Mat(2, 1, CV_64F, m_agghref);
	Mat Bpsi	= Mat(2, 2, CV_64F, m_Bpsi);
	Mat agbh    = Bpsi*agghref;
	Mat E23     = Mat(2, 3, CV_64F, m_E23);
	Mat M_a     = Mat(1, 3, CV_64F, m_a);
	Mat Temp1    = Mat(1, 2, CV_64F, temp1);

	sinphia = agbh.at<double>(1,0)/norm(E23*anb);
	if(sinphia<-1)
		sinphia = -1;
	else if(sinphia>1)
		sinphia = 1;

	phia = asin(sinphia);

	phic = phia + atan2(-anb.at<double>(1,0), -anb.at<double>(2,0));

	double anb1 = -anb.at<double>(1,0);
	double anb2 = -anb.at<double>(2,0);

	double m_b[1][3] = {0, sin(phic), cos(phic)};
	Mat M_b          = Mat(1, 3, CV_64F, m_b);

	Mat a = M_a * anb;
	Mat b = M_b * anb;

	double temp2[1][2] = {a.at<double>(0,0), b.at<double>(0,0)};
	Mat Temp2          = Mat(1, 2, CV_64F, temp2);

	sinthetaa = -agbh.at<double>(0,0)/norm(Temp2);
	if(sinthetaa<-1)
		sinthetaa = -1;
	else if(sinthetaa>1)
		sinthetaa = 1;

	thetaa = asin(sinthetaa);

	thetac = thetaa + atan2(a.at<double>(0,0), -b.at<double>(0,0));

	Mat Temp3 = Temp1*Vgbh;

	Vgbzc = (Vggzref - Temp3.at<double>(0,0))/(cos(phi)*cos(theta));

	double m_cmd[4][1] = {phic, thetac, dpsic, Vgbzc};

	cmd.at<double>(0,0) = phic;
	cmd.at<double>(1,0) = thetac;
	cmd.at<double>(2,0) = dpsic;
	cmd.at<double>(3,0) = Vgbzc;
}

void matrixOmegaE(Mat eulers, Mat &OmegaE){

	double phi   = eulers.at<double>(0,0);
	double theta = eulers.at<double>(1,0);

	OmegaE.at<double>(0,0) = 1;
	OmegaE.at<double>(0,1) = tan(theta)*sin(phi);
	OmegaE.at<double>(0,2) = tan(theta)*cos(phi);
	OmegaE.at<double>(1,0) = 0;
    OmegaE.at<double>(1,1) = cos(phi);
	OmegaE.at<double>(1,2) = -sin(phi);
	OmegaE.at<double>(2,0) = 0;
	OmegaE.at<double>(2,1) = sin(phi)/cos(theta);
	OmegaE.at<double>(2,2) = cos(phi)/cos(theta);

}

void matrixOmega(Mat omegab, Mat &Omega){

	double omegax = omegab.at<double>(0,0);
	double omegay = omegab.at<double>(1,0);
	double omegaz = omegab.at<double>(2,0);

	Omega.at<double>(0,0) = 0;
	Omega.at<double>(0,1) = -omegaz;
	Omega.at<double>(0,2) = omegay;
	Omega.at<double>(1,0) = omegaz;
    Omega.at<double>(1,1) = 0;
	Omega.at<double>(1,2) = -omegax;
	Omega.at<double>(2,0) = -omegay;
	Omega.at<double>(2,1) = omegax;
	Omega.at<double>(2,2) = 0;

}

void matrixBgb(Mat u, Mat &Bgb){

	double phi   = u.at<double>(0,0);
	double theta = u.at<double>(1,0);
	double psi   = u.at<double>(2,0);

	Bgb.at<double>(0,0) = cos(theta)*cos(psi);
	Bgb.at<double>(0,1) = cos(theta)*sin(psi);
	Bgb.at<double>(0,2) = -sin(theta);
	Bgb.at<double>(1,0) = -cos(phi)*sin(psi)+sin(phi)*sin(theta)*cos(psi);
    Bgb.at<double>(1,1) = cos(phi)*cos(psi)+sin(phi)*sin(theta)*sin(psi);
	Bgb.at<double>(1,2) = sin(phi)*cos(theta);
	Bgb.at<double>(2,0) = sin(phi)*sin(psi)+cos(phi)*sin(theta)*cos(psi);
	Bgb.at<double>(2,1) = -sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi);
	Bgb.at<double>(2,2) = cos(phi)*cos(theta);
}

void qmodel(Mat u, Mat &y){

	double m_Vgb[3][1]     = {u.at<double>(0,0), u.at<double>(2,0), u.at<double>(4,0)};
	double m_dVgb[3][1]    = {u.at<double>(1,0), u.at<double>(3,0), u.at<double>(5,0)};
	double m_eulers[3][1]  = {u.at<double>(6,0), u.at<double>(8,0), u.at<double>(10,0)};
	double m_deulers[3][1] = {u.at<double>(7,0), u.at<double>(9,0), u.at<double>(11,0)};
	double m_e3[3][1]      = {0, 0, 1};
	double m_OmegaE[3][3]  = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
	double m_Omega[3][3]   = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
	double m_Bgb[3][3]     = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};

	double phi   = u.at<double>(6,0);
	double theta = u.at<double>(8,0);
	double psi   = u.at<double>(10,0);
	double g     = 9.781;
	double n_phi, n_theta, n_psi;

	Mat Vgb     = Mat(3, 1, CV_64F, m_Vgb);
	Mat dVgb    = Mat(3, 1, CV_64F, m_dVgb);
	Mat eulers  = Mat(3, 1, CV_64F, m_eulers);
	Mat deulers = Mat(3, 1, CV_64F, m_deulers);
	Mat e3      = Mat(3, 1, CV_64F, m_e3);
	Mat OmegaE  = Mat(3, 3, CV_64F, m_OmegaE);
	Mat Omega   = Mat(3, 3, CV_64F, m_Omega);
	Mat Bgb     = Mat(3, 3, CV_64F, m_Bgb);

	n_phi   = atan2(sin(phi),cos(phi));
	n_theta = atan2(sin(theta),cos(theta));
	n_psi   = atan2(sin(psi),cos(psi));

	double m_n_eulers[3][1] = {n_phi, n_theta, n_psi};
	Mat n_eulers = Mat(3, 1, CV_64F, m_n_eulers);

	matrixOmegaE(n_eulers, OmegaE);
	Mat omegab = OmegaE.inv()*deulers;

	matrixBgb(n_eulers, Bgb);
	matrixOmega(omegab, Omega);
	Mat Vgg   = Bgb.t()*Vgb;
	Mat anb   = dVgb + Omega*Vgb - Bgb*g*e3;

	y.at<double>(0,0)  = Vgb.at<double>(0,0);
	y.at<double>(1,0)  = Vgb.at<double>(1,0);
	y.at<double>(2,0)  = Vgb.at<double>(2,0);
	y.at<double>(3,0)  = omegab.at<double>(0,0);
	y.at<double>(4,0)  = omegab.at<double>(1,0);
	y.at<double>(5,0)  = omegab.at<double>(2,0);
	y.at<double>(6,0)  = n_eulers.at<double>(0,0);
	y.at<double>(7,0)  = n_eulers.at<double>(1,0);
	y.at<double>(8,0)  = n_eulers.at<double>(2,0);
	y.at<double>(9,0)  = Vgg.at<double>(0,0);
	y.at<double>(10,0) = Vgg.at<double>(1,0);
	y.at<double>(11,0) = Vgg.at<double>(2,0);
	y.at<double>(12,0) = anb.at<double>(0,0);
	y.at<double>(13,0) = anb.at<double>(1,0);
	y.at<double>(14,0) = anb.at<double>(2,0);

}

void Runge_Kutta_Mat(Mat A, Mat B, Mat &X_n, double u, double d_t){


	Mat k_1 = A*X_n + B*u;
	Mat k_2 = A*(X_n + d_t*k_1/2) + B*u;
	Mat k_3 = A*(X_n + d_t*k_2/2) + B*u;
	Mat k_4 = A*(X_n + d_t*k_3) + B*u;

	X_n = X_n + d_t/6*(k_1 + 2*k_2 + 2*k_3 + k_4);

}

void Runge_Kutta(double a, double b, double &x_n, double u, double d_t){


	double k_1 = a*x_n + b*u;
	double k_2 = a*(x_n + d_t*k_1/2) + b*u;
	double k_3 = a*(x_n + d_t*k_2/2) + b*u;
	double k_4 = a*(x_n + d_t*k_3) + b*u;

	x_n = x_n + d_t/6*(k_1 + 2*k_2 + 2*k_3 + k_4);
}

void Quad_model(Mat cmd, Mat &X_n_phi, Mat &X_n_theta, double &y_n_for, double &y_n_lat, double &y_n_heav, double &y_n_head, double &x_pos, double &y_pos, double &z_pos, Mat &ya){

	double g          = 9.781;
	double d_t        = 0.02;

	double forward_A  = -0.09508;
	double forward_B  = -8.661;
	double y_dot_for  = 0;

	double lateral_A  = -0.09508;
	double lateral_B  = 8.661;
	double y_dot_lat  = 0;

	double heave_A    = -2.32033;
	double heave_B    = -13.3506;
	double y_dot_heav = 0;

	double heading_A  = 0;
	double heading_B  = 3.37205;
	double y_dot_head = 0;

/*    double m_cyclic_A[4][4] =
	{{-28, 1, 0, 0},
	{-459, 0, 1, 0},
	{-5691, 0, 0, 1},
	{-15750, 0, 0, 0}};*/
	double m_cyclic_A[4][4] =
	       {{      0,     1,      0,      0},
	       {      0,     0,      1,      0},
	       {      0,     0,      0,      1},
	       { -15750, -5691, -458.9, -27.68}};

	double m_cyclic_B[4][1] = {0, 0, 0, 9688};
	double m_cyclic_C[2][4] =
	{{1, 0, 0, 0},
	{0, 1, 0, 0}};
	double m_Y_n_phi[2][1]     = {0, 0};
	double m_Y_n_theta[2][1]   = {0, 0};
	double m_y[15][1]          =
	{0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0,
	 0, 0, 0};
	double m_u[12][1]          =
	{0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0};

	Mat cyclic_A    = Mat(4, 4, CV_64F, m_cyclic_A);
	Mat cyclic_B    = Mat(4, 1, CV_64F, m_cyclic_B);
	Mat cyclic_C    = Mat(2, 4, CV_64F, m_cyclic_C);
	Mat Y_n_phi     = Mat(2, 1, CV_64F, m_Y_n_phi);
	Mat Y_n_theta   = Mat(2, 1, CV_64F, m_Y_n_theta);
	Mat y           = Mat(15, 1, CV_64F, m_y);
	Mat u           = Mat(12, 1, CV_64F, m_u);

	double u_phi   = cmd.at<double>(0,0);
	double u_theta = cmd.at<double>(1,0);
	double u_head  = cmd.at<double>(2,0);
	double u_heav  = cmd.at<double>(3,0);

	Runge_Kutta_Mat(cyclic_A, cyclic_B, X_n_phi, u_phi, d_t);
	Y_n_phi  = cyclic_C*X_n_phi;

	Runge_Kutta_Mat(cyclic_A, cyclic_B, X_n_theta, u_theta, d_t);
	Y_n_theta  = cyclic_C*X_n_theta;

	Runge_Kutta(heading_A, heading_B, y_n_head, u_head, d_t);
	y_dot_head = u_head*heading_B;

	Runge_Kutta(heave_A, heave_B, y_n_heav, u_heav, d_t);
	y_dot_heav = heave_A*y_n_heav + heave_B*u_heav;

    double u_for = Y_n_theta.at<double>(0,0);
	double u_lat = Y_n_phi.at<double>(0,0);

	Runge_Kutta(forward_A, forward_B, y_n_for, u_for, d_t);
	y_dot_for = forward_A*y_n_for + forward_B*u_for;

	Runge_Kutta(lateral_A, lateral_B, y_n_lat, u_lat, d_t);
	y_dot_lat = lateral_A*y_n_lat + lateral_B*u_lat;

	u.at<double>(0,0)  = y_n_for;
	u.at<double>(1,0)  = y_dot_for;
	u.at<double>(2,0)  = y_n_lat;
	u.at<double>(3,0)  = y_dot_lat;
	u.at<double>(4,0)  = y_n_heav;
	u.at<double>(5,0)  = y_dot_heav;
	u.at<double>(6,0)  = Y_n_phi.at<double>(0,0);
	u.at<double>(7,0)  = Y_n_phi.at<double>(1,0);
	u.at<double>(8,0)  = Y_n_theta.at<double>(0,0);
	u.at<double>(9,0)  = Y_n_theta.at<double>(1,0);
	u.at<double>(10,0) = y_n_head;
	u.at<double>(11,0) = y_dot_head;

	qmodel(u, y);

	double u_vel = y.at<double>(9,0);
	double v_vel = y.at<double>(10,0);
	double w_vel = y.at<double>(11,0);

	Runge_Kutta(0, 1, x_pos, u_vel, d_t);
	Runge_Kutta(0, 1, y_pos, v_vel, d_t);
	Runge_Kutta(0, 1, z_pos, w_vel, d_t);

	ya.at<double>(0,0) = y.at<double>(0,0);
	ya.at<double>(1,0) = y.at<double>(1,0);
	ya.at<double>(2,0) = y.at<double>(2,0);
	ya.at<double>(3,0) = y.at<double>(3,0);
	ya.at<double>(4,0) = y.at<double>(4,0);
	ya.at<double>(5,0) = y.at<double>(5,0);
	ya.at<double>(6,0) = y.at<double>(6,0);
	ya.at<double>(7,0) = y.at<double>(7,0);
	ya.at<double>(8,0)  = y.at<double>(8,0);
	ya.at<double>(9,0)  = x_pos;
	ya.at<double>(10,0) = y_pos;
	ya.at<double>(11,0) = z_pos;
	ya.at<double>(12,0) = y.at<double>(9,0);
	ya.at<double>(13,0) = y.at<double>(10,0);
	ya.at<double>(14,0) = y.at<double>(11,0);
	ya.at<double>(15,0) = y.at<double>(12,0);
	ya.at<double>(16,0) = y.at<double>(13,0);
	ya.at<double>(17,0) = y.at<double>(14,0);

}

/*void SetFormationInitialStateAll()
{
	FMC1.N=3;
	FMC1.Accmax=5;
	FMC1.Accmin=-5;
	FMC1.Vmax=2;
	FMC1.Vmin=-2;
	FMC1.dpsimax=1.8;
	FMC1.dpsimin=-1.8;
	FMC1.samptime=0.2;


 Shape of formation
	FMC1.fshape[0]=-4;
	FMC1.fshape[1]=10;
	FMC1.fshape[2]=0;
	FMC1.fshape[3]=0;
	FMC1.fshape[4]=0;
	FMC1.fshape[5]=0;
	FMC1.fshape[6]=-4;
	FMC1.fshape[7]=-10;
	FMC1.fshape[8]=0;
	FMC1.fshape[9]=0;
	FMC1.fshape[10]=0;
	FMC1.fshape[11]=0;

// initial position & velocity

	Pmeasureall[0]=0;
	Pmeasureall[1]=5;
	Pmeasureall[2]=0;
	Pmeasureall[3]=0;
	Pmeasureall[4]=0;
	Pmeasureall[5]=0;
	Pmeasureall[6]=0;
	Pmeasureall[7]=-5;
	Pmeasureall[8]=0;
	Pmeasureall[9]=0;
	Pmeasureall[10]=0;
	Pmeasureall[11]=0;

	Vmeasureall[0]=0;
	Vmeasureall[1]=0;
	Vmeasureall[2]=0;
	Vmeasureall[3]=0;
	Vmeasureall[4]=0;
	Vmeasureall[5]=0;
	Vmeasureall[6]=0;
	Vmeasureall[7]=0;
	Vmeasureall[8]=0;
	Vmeasureall[9]=0;
	Vmeasureall[10]=0;
	Vmeasureall[11]=0;

	Vdesireall[0]=0;
	Vdesireall[1]=0;
	Vdesireall[2]=0;
	Vdesireall[3]=0;
	Vdesireall[4]=0;
	Vdesireall[5]=0;
	Vdesireall[6]=0;
	Vdesireall[7]=0;
	Vdesireall[8]=0;
	Vdesireall[9]=0;
	Vdesireall[10]=0;
	Vdesireall[11]=0;

	Psimeasureall[0]=0;
	Psimeasureall[1]=0;
	Psimeasureall[2]=0;
	Psimeasureall[3]=0;

	UAVcontrol.Accgxc=0;
	UAVcontrol.Accgyc=0;
	UAVcontrol.Wc=0;
	UAVcontrol.dpsic=0;


//connectivity
	UAV1cnt=1;
	UAV2cnt=1;
	UAV3cnt=1;
}*/

/*
void FormationSimulation()
{
	double g				   = 9.781;         // Gravational acceleration
	double temp				   = 0;				// Temporary value to save reference into vector
	int counter				   = 0;				// Counter used for print data to txt file
	int i					   = 0;
	int k					   = 0;
	int id;

	double m_DCgain[4][4]      =
	{{1.6257, 0, 0, 0},
	{0, 1.6257, 0, 0},
	{0, 0, 0.29656, 0},
	{0, 0, 0, -0.1738}};

	Mat DCgain      = Mat(4, 4, CV_64F, m_DCgain);

//UAV 1
	double y_n_for_UAV		   = 0;             // Ground velocity 'u' in the body frame
	double y_n_lat_UAV		   = 0;			    // Ground velocity 'v' in the body frame
	double y_n_heav_UAV	    = 0;				// Ground velocity 'w' in the body frame
	double y_n_head_UAV	    = 0;			    // Euler angles 'psi'
	double m_X_n_phi_UAV[4][1]= {0, 0, 0, 0};  // Euler angles 'phi'
	double m_X_n_theta_UAV[4][1]= {0, 0, 0, 0};  // Euler angles 'theta'
	double x_pos_UAV		   = 0;				// Position 'x' in the ground frame
	double y_pos_UAV		   = 5;				// Position 'y' in the ground frame
	double z_pos_UAV	       = 0;				// Position 'z' in the ground frame
	double m_ya_UAV[18][1]    =
	{0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 5, 0,
	 0, 0, 0, 0, 0, -g};						// Output feedback matrix initial values
	double m_ref_UAV[4][1]    = {0, 0, 0, 0};  // Reference in ref(1:4,1):=[agghref(1:2,1)(m/s^2); Vggzref(m/s); dpsic(rad/s)];
	double m_cmd_UAV[4][1]    = {0, 0, 0, 0};  // Control signals from the command convertor


	Mat ya_UAV	    = Mat(18, 1, CV_64F, m_ya_UAV);
	Mat ref_UAV	= Mat(4, 1, CV_64F, m_ref_UAV);
	Mat X_n_phi_UAV = Mat(4, 1, CV_64F, m_X_n_phi_UAV);
	Mat X_n_theta_UAV = Mat(4, 1, CV_64F, m_X_n_theta_UAV); // Construct matrix class in OpenCV
	Mat cmd_UAV       = Mat(4, 1, CV_64F, m_cmd_UAV);  // Construct matrix class in OpenCV

// UAV 1
		id=1;
		Formationcontrol(id);

		ref_UAV.at<double>(0,0) = UAVcontrol.Accgxc;
		ref_UAV.at<double>(1,0) = UAVcontrol.Accgyc;
		ref_UAV.at<double>(2,0) = UAVcontrol.Wc;
		ref_UAV.at<double>(3,0) = UAVcontrol.dpsic;        // control commands to the model

		for (int i=1; i<10; i++)
		{
			cmdconvertor(ya_UAV, ref_UAV, cmd_UAV);
			cmd_UAV = DCgain * cmd_UAV;
			Quad_model(cmd_UAV, X_n_phi_UAV, X_n_theta_UAV, y_n_for_UAV, y_n_lat_UAV, y_n_heav_UAV, y_n_head_UAV, x_pos_UAV, y_pos_UAV, z_pos_UAV, ya_UAV); // Run the iteration
		}

		for (int i=0;i<3;i++)
		{
			Pmeasureall[i]= ya_UAV.at<double>(i+9,0);
			Vmeasureall[i]= ya_UAV.at<double>(i+12,0);
		}
		Psimeasureall[0]= ya_UAV.at<double>(8,0);
}
*/

