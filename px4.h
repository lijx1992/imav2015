//im8.h
//this is the header file for function of IMU - IG500N

#ifndef PX4_H_
#define PX4_H_

#include <sys/select.h>
#include <stdio.h>

#include "thread.h"



struct PX4PACK {
	unsigned char type;

	double  a;
	double  b;
	double  c;

	double  p;
	double  q;
	double  r;

	double  acx;
	double  acy;
	double  acz;

	double  u;
	double  v;
	double  w;

	double  longitude;
	double  latitude;
	double  altitude;

	uint32_t gpstime;
	uint8_t gpsinfo;
	uint8_t nGPS;

	uint32_t pressure;

	//added for UTC time
	uint8_t year, month, day, hour, minutes, seconds;
	uint32_t nanoseconds;
};





#endif				//IM8_H_

